/******************************************************************************

                               Copyright (c) 2012
                            Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
/*
 * $Id: scexempt.c 11794 2011-10-23 12:54:42Z kashani $
 *
 * 
 *
 * 20/40 coexistence feature
 * Provides transition between modes (20MHz->20/40MHz and vice versa)
 * 
 * AP:
 * The scan exemption policy manager will deal with the requests by the associated stations to be exempt 
 * from the periodic OBSS scanning. 
 * Its operation will depend on the current value of a number of MIBs.
 *
 * STA:
 * The scan exemption request / response manager will generate scan exemption requests 
 * and process the respective responses from the associated AP
 *
 * This file relates to both (AP and STA)
 */
#include "mtlkinc.h"

#define COEX_20_40_C
// This define is necessary for coex20_40priv.h file to compile successfully
#include "coex20_40priv.h"
#include "mtlk_param_db.h"

#define LOG_LOCAL_GID   GID_COEX
#define LOG_LOCAL_FID   3

#define DEFAULT_MIN_NUM_OF_NON_EXEMPTED_STA 1
#define MIN_MIN_NUM_OF_NON_EXEMPTED_STA 1
#define MAX_MIN_NUM_OF_NON_EXEMPTED_STA 4

/**************************************************************************************/
/****************************************  AP  ****************************************/
/**************************************************************************************/

static mtlk_sta_exemption_descriptor_t *_mtlk_sepm_find_empty_slot(mtlk_scan_exemption_policy_manager_t *sepm_mgr);
static mtlk_sta_exemption_descriptor_t *_mtlk_sepm_find_first_exempted_station_slot(mtlk_scan_exemption_policy_manager_t *sepm_mgr);
static mtlk_sta_exemption_descriptor_t *_mtlk_sepm_find_slot_by_addr(mtlk_scan_exemption_policy_manager_t *sepm_mgr, const IEEE_ADDR *addr);
static uint32 _mtlk_sepm_count_non_exempted_stations(mtlk_scan_exemption_policy_manager_t *sepm_mgr, const IEEE_ADDR *exception);
static uint32 _mtlk_sepm_count_legacy_stations(mtlk_scan_exemption_policy_manager_t *sepm_mgr);
static uint32 _mtlk_sepm_count_intolerant_stations(mtlk_scan_exemption_policy_manager_t *sepm_mgr);

/* Initialization & cleanup */

int __MTLK_IFUNC mtlk_sepm_init (mtlk_scan_exemption_policy_manager_t *sepm_mgr,
  struct _mtlk_20_40_coexistence_sm *parent_csm, mtlk_20_40_csm_xfaces_t *xfaces, uint32 descriptor_array_size)
{
  int res = MTLK_ERR_NO_RESOURCES;

  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(parent_csm != NULL);
  MTLK_ASSERT(descriptor_array_size > 0);

  sepm_mgr->parent_csm = parent_csm;
  sepm_mgr->xfaces = xfaces;
  sepm_mgr->sta_exemption_descriptors = mtlk_osal_mem_alloc(descriptor_array_size * sizeof(mtlk_sta_exemption_descriptor_t), MTLK_MEM_TAG_COEX_20_40);
  if (sepm_mgr->sta_exemption_descriptors != NULL)
  {
    sepm_mgr->descriptor_array_size = descriptor_array_size;
    memset(sepm_mgr->sta_exemption_descriptors, 0, descriptor_array_size * sizeof(mtlk_sta_exemption_descriptor_t));
    res = MTLK_ERR_OK;
  }
  else
  {
    ELOG_V("ERROR: sta_exemption_descriptors memory allocation failed!");
  }
  sepm_mgr->min_num_of_non_exempted_sta = DEFAULT_MIN_NUM_OF_NON_EXEMPTED_STA;

  return res;
}

void __MTLK_IFUNC mtlk_sepm_cleanup (mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(sepm_mgr->parent_csm != NULL);

  if (sepm_mgr->sta_exemption_descriptors)
  {
    mtlk_osal_mem_free(sepm_mgr->sta_exemption_descriptors);
  }
}


/* External functional interfaces (meant only for the parent coexistence state machine) */

void __MTLK_IFUNC mtlk_sepm_register_station (mtlk_scan_exemption_policy_manager_t *sepm_mgr,
  const IEEE_ADDR *sta_addr, BOOL supports_coexistence, BOOL exempt, BOOL intolerant, BOOL legacy)
{
  mtlk_sta_exemption_descriptor_t *new_station_slot;

  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(sta_addr != NULL);

  if (!_mtlk_sepm_find_slot_by_addr(sepm_mgr, sta_addr))
  {
    new_station_slot = _mtlk_sepm_find_empty_slot(sepm_mgr);
    if (new_station_slot)
    {
      new_station_slot->supports_coexistence = supports_coexistence;
      new_station_slot->exempt = exempt;
      new_station_slot->intolerant = intolerant;
      new_station_slot->legacy = legacy;
      memcpy(new_station_slot->sta_addr.au8Addr, sta_addr, sizeof(new_station_slot->sta_addr.au8Addr));
      new_station_slot->slot_used = TRUE;
      if ((new_station_slot->legacy == TRUE) || (new_station_slot->intolerant))
      {
        mtlk_20_40_ap_notify_intolerant_or_legacy_station_connected(sepm_mgr->parent_csm, TRUE /* don't lock, the lock is already acquired by the caller */);
      }
    }
    else
    {
      ELOG_V("Can't find an empty slot for new STA - exemption will never be given to this STA");
    }
  }
}

void __MTLK_IFUNC mtlk_sepm_unregister_station (mtlk_scan_exemption_policy_manager_t *sepm_mgr,
  const IEEE_ADDR *sta_addr)
{
  mtlk_sta_exemption_descriptor_t *unregistered_station_slot;
  mtlk_sta_exemption_descriptor_t *exempted_station_slot;
  mtlk_20_40_coexistence_element coex_el;

  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(sta_addr != NULL);

  unregistered_station_slot = _mtlk_sepm_find_slot_by_addr(sepm_mgr, sta_addr);
  if (unregistered_station_slot)
  {
    unregistered_station_slot->slot_used = FALSE;
    if (unregistered_station_slot->supports_coexistence && !mtlk_20_40_is_intolerance_declared(sepm_mgr->parent_csm))
    { // If the disconnected station didn't support coexistence, it doesn't affect the exemption granting logic.
      // In addition, we're not going to force the connected stations to perform scans if we're are an intolerant AP
      while (_mtlk_sepm_count_non_exempted_stations(sepm_mgr, NULL) < mtlk_sepm_get_min_non_exempted_sta(sepm_mgr))
      { /* Not enough scanning stations remain, we will look for an exempted station that can be requested to start
           periodic OBSS scanning 
           Policy: at least min_num_of_non_exempted_sta should not be exempted */
        exempted_station_slot = _mtlk_sepm_find_first_exempted_station_slot(sepm_mgr);
        if (!exempted_station_slot)
        { // None of the remaining stations can be requested to start scanning for us
          break;
        }
        memset(&coex_el, 0, sizeof(coex_el));
        coex_el.u8OBSSScanningExemptionGrant = FALSE;
        if (mtlk_cefg_send_coexistence_frame(&sepm_mgr->parent_csm->frgen,
                                              &exempted_station_slot->sta_addr,
                                              &coex_el,
                                              NULL) == MTLK_ERR_OK)
        {
          exempted_station_slot->exempt = FALSE;
          mtlk_coex_20_40_inc_cnt(sepm_mgr->parent_csm, MTLK_COEX_20_40_NOF_COEX_EL_SCAN_EXEMPTION_GRANT_CANCELLED);
          mtlk_20_40_set_intolerance_at_first_scan_flag(sepm_mgr->parent_csm, FALSE, TRUE /* don't lock, already locked */);
        }
      }
    }
    if (unregistered_station_slot->legacy || unregistered_station_slot->intolerant)
    {
      if ((_mtlk_sepm_count_legacy_stations(sepm_mgr) == 0) && (_mtlk_sepm_count_intolerant_stations(sepm_mgr) == 0))
      {
        mtlk_20_40_ap_notify_last_40_incapable_station_disconnected(sepm_mgr->parent_csm, TRUE /* don't lock, the lock is already acquired by the caller */);
      }
    }
    memset(unregistered_station_slot->sta_addr.au8Addr, 0, sizeof(unregistered_station_slot->sta_addr.au8Addr));
    unregistered_station_slot->exempt = FALSE;
    unregistered_station_slot->intolerant = FALSE;
    unregistered_station_slot->legacy = FALSE;
  }
}

int __MTLK_IFUNC mtlk_sepm_process_exemption_request (mtlk_scan_exemption_policy_manager_t *sepm_mgr,
  const IEEE_ADDR *sta_addr)
{
  int ret_val = MTLK_ERR_OK;
  mtlk_sta_exemption_descriptor_t *station_slot;
  mtlk_20_40_coexistence_element coex_el;

  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(sta_addr != NULL);
  
  mtlk_coex_20_40_inc_cnt(sepm_mgr->parent_csm, MTLK_COEX_20_40_NOF_COEX_EL_SCAN_EXEMPTION_REQUESTED);

  station_slot = _mtlk_sepm_find_slot_by_addr(sepm_mgr, sta_addr);
  if (station_slot)
  {
    memset(&coex_el, 0, sizeof(coex_el));
    if ((_mtlk_sepm_count_non_exempted_stations(sepm_mgr, NULL) > mtlk_sepm_get_min_non_exempted_sta(sepm_mgr)) || mtlk_20_40_is_intolerance_declared(sepm_mgr->parent_csm))
    { /* If min_non_exempted_sta non-exempt stations exist already or if we are an intolerant AP 9and thus not interested in stations
         scanning for us), we can grant the station's request for exemption
         NOTE: we're not going to check whether the station is currently considered to be exempt
         or not; if it requests an exemption, we will grant one and send the respective coexistence
         frame whenever possible */
      coex_el.u8OBSSScanningExemptionGrant = TRUE;/* filled with FALSE by default */
      coex_el.u8FortyMhzIntolerant = mtlk_20_40_is_intolerance_declared(sepm_mgr->parent_csm);
    }
    ret_val = mtlk_cefg_send_coexistence_frame (&sepm_mgr->parent_csm->frgen,
                                                sta_addr,
                                                &coex_el,
                                                NULL);
    if (ret_val == MTLK_ERR_OK && coex_el.u8OBSSScanningExemptionGrant)
    {
      station_slot->exempt = TRUE;
      mtlk_coex_20_40_inc_cnt(sepm_mgr->parent_csm, MTLK_COEX_20_40_NOF_COEX_EL_SCAN_EXEMPTION_GRANTED);
    }
  }

  return ret_val;
}

BOOL __MTLK_IFUNC mtlk_sepm_register_station_intolerance (mtlk_scan_exemption_policy_manager_t *sepm_mgr,
  const IEEE_ADDR *sta_addr)
{
  BOOL changed = FALSE;
  mtlk_sta_exemption_descriptor_t *station_slot;

  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(sta_addr != NULL);

  station_slot = _mtlk_sepm_find_slot_by_addr(sepm_mgr, sta_addr);
  if (station_slot)
  {
    if (station_slot->intolerant == FALSE)
    {
      station_slot->intolerant = TRUE;
      changed = TRUE;
    }
  }

  return changed;
}

BOOL __MTLK_IFUNC mtlk_sepm_is_assoc_sta (mtlk_scan_exemption_policy_manager_t *sepm_mgr,
  const IEEE_ADDR *sta_addr)
{
  BOOL res = FALSE;

  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(sta_addr != NULL);

  if (_mtlk_sepm_find_slot_by_addr(sepm_mgr, sta_addr))
  {
    res = TRUE;
  }
  return res;
}

BOOL __MTLK_IFUNC mtlk_sepm_is_intolerant_or_legacy_station_connected(mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  BOOL res = FALSE;
  uint32 i;

  MTLK_ASSERT(sepm_mgr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if (sepm_mgr->sta_exemption_descriptors[i].slot_used == TRUE)
    {
      if ((sepm_mgr->sta_exemption_descriptors[i].intolerant == TRUE) || (sepm_mgr->sta_exemption_descriptors[i].legacy == TRUE))
      {
        res = TRUE;
        break;
      }
    }
  }

  return res;
}

BOOL __MTLK_IFUNC mtlk_sepm_is_intolerant_station_connected(mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  BOOL res = FALSE;
  uint32 i;

  MTLK_ASSERT(sepm_mgr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if (sepm_mgr->sta_exemption_descriptors[i].slot_used == TRUE)
    {
      if (sepm_mgr->sta_exemption_descriptors[i].intolerant == TRUE)
      {
        res = TRUE;
        break;
      }
    }
  }

  return res;
}

/* Internal functions */
static mtlk_sta_exemption_descriptor_t *_mtlk_sepm_find_empty_slot(mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  uint32 i;
  mtlk_sta_exemption_descriptor_t *ret_val = NULL;

  MTLK_ASSERT(sepm_mgr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if (sepm_mgr->sta_exemption_descriptors[i].slot_used == FALSE)
    {
      ret_val = &sepm_mgr->sta_exemption_descriptors[i];
      break;
    }
  }

  return ret_val;
}

static mtlk_sta_exemption_descriptor_t *_mtlk_sepm_find_first_exempted_station_slot(mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  uint32 i;
  mtlk_sta_exemption_descriptor_t *ret_val = NULL;

  MTLK_ASSERT(sepm_mgr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if ((sepm_mgr->sta_exemption_descriptors[i].slot_used == TRUE) &&
        (sepm_mgr->sta_exemption_descriptors[i].supports_coexistence == TRUE) &&
        (sepm_mgr->sta_exemption_descriptors[i].exempt == TRUE))
    {
      ret_val = &sepm_mgr->sta_exemption_descriptors[i];
      break;
    }
  }

  return ret_val;
}

static mtlk_sta_exemption_descriptor_t *_mtlk_sepm_find_slot_by_addr(mtlk_scan_exemption_policy_manager_t *sepm_mgr, const IEEE_ADDR *addr)
{
  uint32 i;
  mtlk_sta_exemption_descriptor_t *ret_val = NULL;

  MTLK_ASSERT(sepm_mgr != NULL);
  MTLK_ASSERT(addr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if (sepm_mgr->sta_exemption_descriptors[i].slot_used == TRUE)
    {
      if (!memcmp(sepm_mgr->sta_exemption_descriptors[i].sta_addr.au8Addr, addr->au8Addr, sizeof(sepm_mgr->sta_exemption_descriptors[i].sta_addr.au8Addr)))
      {
        ret_val = &sepm_mgr->sta_exemption_descriptors[i];
        break;
      }
    }
  }

  return ret_val;
}

static uint32 _mtlk_sepm_count_non_exempted_stations(mtlk_scan_exemption_policy_manager_t *sepm_mgr, const IEEE_ADDR *exception)
{
  uint32 i;
  uint32 ret_val = 0;

  MTLK_ASSERT(sepm_mgr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if ((sepm_mgr->sta_exemption_descriptors[i].slot_used == TRUE) &&
        (sepm_mgr->sta_exemption_descriptors[i].supports_coexistence == TRUE))
    {
      if (sepm_mgr->sta_exemption_descriptors[i].exempt == FALSE)
      {
        if (exception)
        { /* We've been instructed to count all stations with one exception, so we first have to check whether the current
             station is the exception */
          if (memcmp(sepm_mgr->sta_exemption_descriptors[i].sta_addr.au8Addr, exception->au8Addr, sizeof(sepm_mgr->sta_exemption_descriptors[i].sta_addr.au8Addr)))
          {
            ret_val ++;
          }
        }
        else
        { /* We've been instructed to count all stations without exception */
          ret_val ++;
        }
      }
    }
  }

  return ret_val;
}

static uint32 _mtlk_sepm_count_legacy_stations(mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  uint32 i;
  uint32 ret_val = 0;

  MTLK_ASSERT(sepm_mgr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if ((sepm_mgr->sta_exemption_descriptors[i].slot_used == TRUE) &&
        (sepm_mgr->sta_exemption_descriptors[i].legacy == TRUE))
    {
        ret_val ++;
    }
  }

  return ret_val;
}

static uint32 _mtlk_sepm_count_intolerant_stations(mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  uint32 i;
  uint32 ret_val = 0;

  MTLK_ASSERT(sepm_mgr != NULL);

  for (i = 0; i < sepm_mgr->descriptor_array_size; i ++)
  {
    if ((sepm_mgr->sta_exemption_descriptors[i].slot_used == TRUE) &&
        (sepm_mgr->sta_exemption_descriptors[i].intolerant == TRUE))
    {
      ret_val ++;
    }
  }

  return ret_val;
}

uint32 __MTLK_IFUNC mtlk_sepm_get_min_non_exempted_sta (mtlk_scan_exemption_policy_manager_t *sepm_mgr)
{
  MTLK_ASSERT(NULL != sepm_mgr);

  return sepm_mgr->min_num_of_non_exempted_sta;
}

int __MTLK_IFUNC mtlk_sepm_set_min_non_exempted_sta (mtlk_scan_exemption_policy_manager_t *sepm_mgr, uint32 min_num_of_non_exempted_sta)
{
  int res = MTLK_ERR_OK;

  MTLK_ASSERT(NULL != sepm_mgr);

  if (min_num_of_non_exempted_sta >= MIN_MIN_NUM_OF_NON_EXEMPTED_STA && min_num_of_non_exempted_sta <= MAX_MIN_NUM_OF_NON_EXEMPTED_STA)
  {
    sepm_mgr->min_num_of_non_exempted_sta = min_num_of_non_exempted_sta;
  }
  else
  {
    res = MTLK_ERR_VALUE;
  }

return MTLK_ERR_OK;
}

/**************************************************************************************/
/****************************************  STA  ***************************************/
/**************************************************************************************/

/* Initialization & cleanup */

static uint32 _mtlk_obsm_scan_timeout_callback(mtlk_osal_timer_t *timer,
  mtlk_handle_t clb_usr_data);
static void _mtlk_sepm_scan_completed_notification_callback(mtlk_handle_t context);
static int mtlk_obsm_send_exemption_request(mtlk_obss_scan_manager_t *obsm_mgr);
static int _mtlk_obsm_set_scan_timer(mtlk_obss_scan_manager_t *obsm_mgr);
static void _mtlk_obsm_cancel_scan_timer(mtlk_obss_scan_manager_t *obsm_mgr);
static void _mtlk_obsm_bss_info_enumerator_callback(mtlk_handle_t context,
  mtlk_20_40_bss_info_t *bss_info);
static uint8 _mtlk_obsm_extract_operating_class_from_bss_info(mtlk_obss_scan_manager_t *obsm_mgr,
  mtlk_20_40_bss_info_t *bss_info);
static BOOL _mtlk_obsm_find_descriptor_ptr(mtlk_obss_scan_manager_t *obsm_mgr,
  uint8 operating_class, UMI_INTOLERANT_CHANNEL_DESCRIPTOR **intolerant_channel_desc);

MTLK_INIT_STEPS_LIST_BEGIN(obsm_mgr)
  MTLK_INIT_STEPS_LIST_ENTRY(obsm_mgr, SCAN_TIMER)
MTLK_INIT_INNER_STEPS_BEGIN(obsm_mgr)
MTLK_INIT_STEPS_LIST_END(obsm_mgr);

int __MTLK_IFUNC mtlk_obsm_init(mtlk_obss_scan_manager_t *obsm_mgr,
  struct _mtlk_20_40_coexistence_sm *parent_csm, mtlk_20_40_csm_xfaces_t *xfaces)
{
  MTLK_ASSERT(obsm_mgr != NULL);
  MTLK_ASSERT(parent_csm != NULL);
  MTLK_ASSERT(xfaces != NULL);

  ILOG1_V("Initializing OBSS scan manager");
  obsm_mgr->parent_csm = parent_csm;
  obsm_mgr->xfaces = xfaces;
  obsm_mgr->state = MTLK_OBSM_STATE_NOT_STARTED;
  obsm_mgr->interested_in_exemption = FALSE;
  obsm_mgr->scan_in_progress = FALSE;
  obsm_mgr->last_scan_ts = 0;
  obsm_mgr->next_scan_due_time_ts = 0;
  memset(&obsm_mgr->coex_el, 0, sizeof(obsm_mgr->coex_el));
  memset(&obsm_mgr->scan_results, 0, sizeof(obsm_mgr->scan_results));
  (*obsm_mgr->xfaces->register_scan_completion_notification_callback)(HANDLE_T(obsm_mgr), obsm_mgr->xfaces->context, &_mtlk_sepm_scan_completed_notification_callback);

  MTLK_INIT_TRY(obsm_mgr, MTLK_OBJ_PTR(obsm_mgr))
    MTLK_INIT_STEP(obsm_mgr, SCAN_TIMER, MTLK_OBJ_PTR(obsm_mgr),
                   mtlk_osal_timer_init,
                   (&obsm_mgr->scan_timer, _mtlk_obsm_scan_timeout_callback, (mtlk_handle_t)obsm_mgr));
  MTLK_INIT_FINALLY(obsm_mgr, MTLK_OBJ_PTR(obsm_mgr))
  MTLK_INIT_RETURN(obsm_mgr, MTLK_OBJ_PTR(obsm_mgr), mtlk_obsm_cleanup, (obsm_mgr))
}

int __MTLK_IFUNC mtlk_obsm_start(mtlk_obss_scan_manager_t *obsm_mgr)
{
  MTLK_ASSERT(obsm_mgr != NULL);

 ILOG1_V("Starting OBSS scan manager");
 switch (obsm_mgr->state)
  {
    case MTLK_OBSM_STATE_NOT_STARTED:
      obsm_mgr->state = MTLK_OBSM_STATE_STARTED;
      break;
    default:
      /* Nothing to do */
      break;
  }

  return MTLK_ERR_OK;
}

void __MTLK_IFUNC mtlk_obsm_stop(mtlk_obss_scan_manager_t *obsm_mgr)
{
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG1_V("Stopping OBSS scan manager");

  _mtlk_obsm_cancel_scan_timer(obsm_mgr);
  switch (obsm_mgr->state)
  {
    case MTLK_OBSM_STATE_STARTED:
    case MTLK_OBSM_STATE_CONNECTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_REQUESTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_ACTIVE:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_OVERRIDDEN:
      obsm_mgr->state = MTLK_OBSM_STATE_NOT_STARTED;
      break;
    default:
      /* Nothing to do */
      break;
  }
}

void __MTLK_IFUNC mtlk_obsm_cleanup(mtlk_obss_scan_manager_t *obsm_mgr)
{
  MTLK_ASSERT(obsm_mgr != NULL);
  MTLK_ASSERT(obsm_mgr->parent_csm != NULL);

  ILOG1_V("Cleaning up OBSS scan manager");

  (*obsm_mgr->xfaces->register_scan_completion_notification_callback)(HANDLE_T(obsm_mgr), obsm_mgr->xfaces->context, NULL);

  MTLK_CLEANUP_BEGIN(obsm_mgr, MTLK_OBJ_PTR(obsm_mgr))
    MTLK_CLEANUP_STEP(obsm_mgr, SCAN_TIMER, MTLK_OBJ_PTR(obsm_mgr),
                      mtlk_osal_timer_cleanup,
                      (&obsm_mgr->scan_timer));
  MTLK_CLEANUP_END(obsm_mgr, MTLK_OBJ_PTR(obsm_mgr))
}


/* External functional interfaces (meant only for the parent coexistence state machine) */

void __MTLK_IFUNC mtlk_obsm_sta_notify_connection_to_ap(mtlk_obss_scan_manager_t *obsm_mgr, BOOL supports_coexistence, BOOL scan_exemption_received)
{ /* This function assumes the lock is already acquired by the caller */
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG1_V("The station has connected to an AP");

  switch (obsm_mgr->state)
  {
    case MTLK_OBSM_STATE_STARTED:
      if (supports_coexistence == TRUE)
      {
        if (scan_exemption_received == TRUE)
        {
          obsm_mgr->state = MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_ACTIVE;
        }
        else
        {
          obsm_mgr->state = MTLK_OBSM_STATE_CONNECTED;
          _mtlk_obsm_set_scan_timer(obsm_mgr);
        }
      }
      break;
    default:
      /* Nothing to do */
      break;
  }
}

void __MTLK_IFUNC mtlk_obsm_sta_notify_disconnection_from_ap(mtlk_obss_scan_manager_t *obsm_mgr)
{ /* This function assumes the lock is already acquired by the caller */
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG1_V("The station has disconnected from the AP");

  switch (obsm_mgr->state)
  {
    case MTLK_OBSM_STATE_CONNECTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_REQUESTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_OVERRIDDEN:
      obsm_mgr->state = MTLK_OBSM_STATE_STARTED;
      _mtlk_obsm_cancel_scan_timer(obsm_mgr);
      break;
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_ACTIVE:
      obsm_mgr->state = MTLK_OBSM_STATE_STARTED;
      break;
    default:
      /* Nothing to do */
      break;
  }
}

void __MTLK_IFUNC mtlk_obsm_sta_notify_switch_to_20_mode(mtlk_obss_scan_manager_t *obsm_mgr, uint16 channel)
{ /* This function assumes the lock is already acquired by the caller */
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG1_D("The station received the instruction to move to 20 MHz mode (channel %d)", channel);
}

void __MTLK_IFUNC mtlk_obsm_sta_notify_switch_to_40_mode(mtlk_obss_scan_manager_t *obsm_mgr, uint16 primary_channel, int secondary_channel_offset)
{ /* This function assumes the lock is already acquired by the caller */
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG1_DD("The station received the instruction to move to 40 MHz mode (primary channel %d, secondary channel offset %d)", primary_channel, secondary_channel_offset);
}

void __MTLK_IFUNC mtlk_obsm_set_request_scan_exemption_flag (mtlk_obss_scan_manager_t *obsm_mgr, BOOL must_request)
{
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG2_D("Setting request exemption flag to: %d", must_request);

  mtlk_20_40_lock(obsm_mgr->parent_csm);

  if (must_request && (obsm_mgr->interested_in_exemption == FALSE))
  {
    obsm_mgr->interested_in_exemption = TRUE;
    switch (obsm_mgr->state)
    {
      case MTLK_OBSM_STATE_CONNECTED:
        if (mtlk_obsm_send_exemption_request(obsm_mgr) == MTLK_ERR_OK)
        {
          obsm_mgr->state = MTLK_OBSM_STATE_CONNECTED_EXEMPTION_REQUESTED;
        }
        break;
      default:
        /* Nothing to do */
        break;
    }
  }
  else if (!must_request && (obsm_mgr->interested_in_exemption == TRUE))
  {
    obsm_mgr->interested_in_exemption = FALSE;
    /* NOTE: if we have already requested or received exemption, we will continue enjoying it even though we're not
       required to request exemption any more. The exemption will be effective until canceled by the AP. As a result,
       the current state will not change */
  }

  mtlk_20_40_unlock(obsm_mgr->parent_csm);
}

int __MTLK_IFUNC mtlk_obsm_process_response(mtlk_obss_scan_manager_t *obsm_mgr, BOOL exemption_granted)
{ /* This function assumes the lock is already acquired by the caller */
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG2_D("Processing the response. exemption_granted = %d", exemption_granted);

  if (exemption_granted)
  {
    _mtlk_obsm_cancel_scan_timer(obsm_mgr);
    switch (obsm_mgr->state)
    {
      case MTLK_OBSM_STATE_CONNECTED:
      case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_REQUESTED:
      case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_OVERRIDDEN:
        obsm_mgr->state = MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_ACTIVE;
        break;
      default:
        /* Nothing to do */
        break;
    }
  }
  else
  {
    switch (obsm_mgr->state)
    {
      case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_ACTIVE:
        _mtlk_obsm_set_scan_timer(obsm_mgr);
      case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_REQUESTED:
      case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_OVERRIDDEN:
        obsm_mgr->state = MTLK_OBSM_STATE_CONNECTED;
        break;
      default:
        /* Nothing to do */
        break;
    }
  }

  return MTLK_ERR_OK;
}

void __MTLK_IFUNC mtlk_obsm_prepare_and_send_obss_scan_report(mtlk_obss_scan_manager_t *obsm_mgr, const IEEE_ADDR *addr, BOOL dont_lock)
{
  int i;

  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG2_V("Preparing and sending OBSS scan report");

  if (dont_lock == FALSE)
  {
    mtlk_20_40_lock(obsm_mgr->parent_csm);
  }

  memset(&obsm_mgr->coex_el, 0, sizeof(obsm_mgr->coex_el));
  memset(&obsm_mgr->scan_results, 0, sizeof(obsm_mgr->scan_results));
  (*obsm_mgr->parent_csm->xfaces.enumerate_bss_info)(
    (mtlk_handle_t)obsm_mgr,
    obsm_mgr->parent_csm->xfaces.context,
    &_mtlk_obsm_bss_info_enumerator_callback,
    mtlk_20_40_calc_transition_timer(obsm_mgr->parent_csm));
  
  obsm_mgr->coex_el.u8FortyMhzIntolerant = mtlk_20_40_is_intolerance_declared(obsm_mgr->parent_csm);
  obsm_mgr->coex_el.u8TwentyMhzBSSWidthRequest = mtlk_20_40_is_coex_el_intolerant_bit_detected(obsm_mgr->parent_csm, TRUE /* ignore timeouts */);
  obsm_mgr->coex_el.u8OBSSScanningExemptionRequest = mtlk_20_40_sta_is_scan_exemption_request_forced(obsm_mgr->parent_csm);
  ILOG1_DDD("COEXISTENCE FRAME DUMP. u8FortyMhzIntolerant = %d, u8TwentyMhzBSSWidthRequest = %d, u8NumberOfIntolerantChannelDescriptors = %d", obsm_mgr->coex_el.u8FortyMhzIntolerant, obsm_mgr->coex_el.u8TwentyMhzBSSWidthRequest, obsm_mgr->scan_results.intolerant_channels_report.u8NumberOfIntolerantChannelDescriptors);
  for (i = 0; i < obsm_mgr->scan_results.intolerant_channels_report.u8NumberOfIntolerantChannelDescriptors; i ++)
  {
    ILOG1_DDDDDDDDDD("ICD[%d]: NOIC: %d: %d, %d, %d, %d, %d, %d, %d, %d", i, obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8NumberOfIntolerantChannels,
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[0],
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[1],
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[2],
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[3],
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[4],
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[5],
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[6],
              obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8IntolerantChannels[7]
    );
  }
  ILOG1_D("COEXISTENCE FRAME DUMP. 20MHz width BSS request field in Coexistence Element = %d (1 means intolerance detected, 0 means no intolerance detected)", obsm_mgr->coex_el.u8TwentyMhzBSSWidthRequest);

  mtlk_cefg_send_coexistence_frame(
    &obsm_mgr->parent_csm->frgen,
    addr,
    &obsm_mgr->coex_el,
    &obsm_mgr->scan_results);

  mtlk_20_40_clear_intolerant_db(obsm_mgr->parent_csm);

  if (dont_lock == FALSE)
  {
    mtlk_20_40_unlock(obsm_mgr->parent_csm);
  }
}

void __MTLK_IFUNC mtlk_obsm_notify_scan_interval_updated(mtlk_obss_scan_manager_t *obsm_mgr, BOOL dont_lock)
{
  mtlk_osal_timestamp_t cur_time_ts;
  mtlk_osal_timestamp_t new_due_time_ts;
  uint32 new_timer_duration;
  uint32 new_due_time;

  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG2_V("OBSS manager was notified that scan interval had been updated");

  if (dont_lock == FALSE)
  {
    mtlk_20_40_lock(obsm_mgr->parent_csm);
  }

  switch (obsm_mgr->state)
  {
    case MTLK_OBSM_STATE_CONNECTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_REQUESTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_OVERRIDDEN:
      cur_time_ts = mtlk_osal_timestamp();
      new_timer_duration = MTLK_OSAL_MSEC_IN_SEC * mtlk_20_40_get_scan_interval(obsm_mgr->parent_csm);
      new_due_time = mtlk_osal_timestamp_to_ms(cur_time_ts) + new_timer_duration;
      new_due_time_ts = mtlk_osal_ms_to_timestamp(new_due_time);
  
      if (mtlk_osal_time_after(obsm_mgr->next_scan_due_time_ts, new_due_time_ts) == TRUE)
      { /* According to the updated value of the scan interval, the next scan must take place earlier
           than currently planned. We'll cancel the scan timer and then reschedule it using the new value
           of the scan interval */
        _mtlk_obsm_cancel_scan_timer(obsm_mgr);
        _mtlk_obsm_set_scan_timer(obsm_mgr);
      }
      break;
    default:
      /* Nothing to do */
      break;
  }

  if (dont_lock == FALSE)
  {
    mtlk_20_40_unlock(obsm_mgr->parent_csm);
  }
}

/* Static functions for internal use */ 

static uint32 _mtlk_obsm_scan_timeout_callback(mtlk_osal_timer_t *timer, mtlk_handle_t clb_usr_data)
{
  mtlk_obss_scan_manager_t *obsm_mgr = (mtlk_obss_scan_manager_t*)clb_usr_data;

  ILOG2_V("Scan timer timeout, initiating OBSS scan");

  mtlk_20_40_lock(obsm_mgr->parent_csm);
  if (obsm_mgr->state != MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_ACTIVE)
  {
    (*obsm_mgr->xfaces->scan_set_background)(obsm_mgr->xfaces->context, TRUE);
    (*obsm_mgr->xfaces->register_scan_completion_notification_callback)(HANDLE_T(obsm_mgr), obsm_mgr->xfaces->context, &_mtlk_sepm_scan_completed_notification_callback);
    obsm_mgr->last_scan_ts = mtlk_osal_timestamp();
    obsm_mgr->scan_in_progress = TRUE;
    (*obsm_mgr->xfaces->scan_async)(obsm_mgr->xfaces->context, FALSE);
  }
  mtlk_20_40_unlock(obsm_mgr->parent_csm);

  return 0; /* Don't reactivate the timer */
}

static void _mtlk_sepm_scan_completed_notification_callback(mtlk_handle_t context)
{
  mtlk_obss_scan_manager_t *obsm_mgr = (mtlk_obss_scan_manager_t*)context;
  IEEE_ADDR ap_addr;

  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG2_V("OBSS scan has completed");

  mtlk_20_40_lock(obsm_mgr->parent_csm);

  (*obsm_mgr->xfaces->register_scan_completion_notification_callback)(context, obsm_mgr->xfaces->context, NULL);
  obsm_mgr->scan_in_progress = FALSE;
  (*obsm_mgr->xfaces->scan_set_background)(obsm_mgr->xfaces->context, FALSE);
  memset(&ap_addr, 0, sizeof(ap_addr));
  mtlk_pdb_get_mac(mtlk_vap_get_param_db(obsm_mgr->parent_csm->xfaces.vap_handle), PARAM_DB_CORE_BSSID, ap_addr.au8Addr);
  mtlk_obsm_prepare_and_send_obss_scan_report(obsm_mgr, &ap_addr, TRUE /* don't lock, already locked */);

  switch (obsm_mgr->state)
  {
    case MTLK_OBSM_STATE_CONNECTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_REQUESTED:
    case MTLK_OBSM_STATE_CONNECTED_EXEMPTION_RECEIVED_OVERRIDDEN:
      _mtlk_obsm_set_scan_timer(obsm_mgr);
      break;
    default:
      /* Nothing to do */
      break;
  }

  mtlk_20_40_unlock(obsm_mgr->parent_csm);
}

static int mtlk_obsm_send_exemption_request(mtlk_obss_scan_manager_t *obsm_mgr)
{  
  mtlk_20_40_coexistence_element coex_el;
  IEEE_ADDR ap_addr;
  
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG2_V("Sending a scan exemption request");

  memset(&ap_addr, 0, sizeof(ap_addr));
  mtlk_pdb_get_mac(mtlk_vap_get_param_db(obsm_mgr->parent_csm->xfaces.vap_handle), PARAM_DB_CORE_BSSID, ap_addr.au8Addr);
  memset(&coex_el, 0, sizeof(coex_el));
  coex_el.u8OBSSScanningExemptionRequest = TRUE;
  return mtlk_cefg_send_coexistence_frame(&obsm_mgr->parent_csm->frgen, &ap_addr, &coex_el, NULL);
}

static int _mtlk_obsm_set_scan_timer(mtlk_obss_scan_manager_t *obsm_mgr)
{
  int res; 
  mtlk_osal_timestamp_t cur_time;
  uint32 timer_duration;
  
  MTLK_ASSERT(obsm_mgr != NULL);

  cur_time = mtlk_osal_timestamp();
  timer_duration = MTLK_OSAL_MSEC_IN_SEC * mtlk_20_40_get_scan_interval(obsm_mgr->parent_csm);
  ILOG2_D("Setting scan timer to fire in %d ms", timer_duration);
  res = mtlk_osal_timer_set(&obsm_mgr->scan_timer, timer_duration);
  if (res == MTLK_ERR_OK)
  {
    uint32 due_time = mtlk_osal_timestamp_to_ms(cur_time) + timer_duration;
    obsm_mgr->next_scan_due_time_ts = mtlk_osal_ms_to_timestamp(due_time);
  }

  return res;
}

static void _mtlk_obsm_cancel_scan_timer(mtlk_obss_scan_manager_t *obsm_mgr)
{
  MTLK_ASSERT(obsm_mgr != NULL);

  ILOG2_V("Cancelling the scan timer");

  mtlk_osal_timer_cancel_sync(&obsm_mgr->scan_timer);
  obsm_mgr->next_scan_due_time_ts = 0;
}

static void _mtlk_obsm_bss_info_enumerator_callback(mtlk_handle_t context,
  mtlk_20_40_bss_info_t *bss_info)
{
  mtlk_obss_scan_manager_t *obsm_mgr = (mtlk_obss_scan_manager_t*)context;
  static uint8 operating_class;
  UMI_INTOLERANT_CHANNEL_DESCRIPTOR *intolerant_channel_desc = NULL;
  
  MTLK_ASSERT(NULL != obsm_mgr);
  MTLK_ASSERT(NULL != bss_info);

  if(!mtlk_20_40_check_rssi_threshold(obsm_mgr->parent_csm, bss_info->max_rssi))
  {
    return;
  }

  if (bss_info->is_2_4 && bss_info->is_ht && bss_info->forty_mhz_intolerant)
  {
    operating_class = _mtlk_obsm_extract_operating_class_from_bss_info(obsm_mgr, bss_info);
    if (_mtlk_obsm_find_descriptor_ptr(obsm_mgr, operating_class, &intolerant_channel_desc))
    {
      if (intolerant_channel_desc->u8NumberOfIntolerantChannels < UMI_MAX_NUMBER_OF_INTOLERANT_CHANNELS)
      { /* We still have room for more channels in the descriptor */
        uint8 i;
        BOOL found = FALSE;
        for (i = 0; i < intolerant_channel_desc->u8NumberOfIntolerantChannels; i ++)
        {
          if (intolerant_channel_desc->u8IntolerantChannels[i] == bss_info->channel)
          { /* The channel already appears in the descriptor */
            found = TRUE;
            break;
          }
        }
        if (!found)
        {
          intolerant_channel_desc->u8IntolerantChannels[intolerant_channel_desc->u8NumberOfIntolerantChannels] = bss_info->channel;
          intolerant_channel_desc->u8NumberOfIntolerantChannels ++;
        }
      }
    }
  }
}

static uint8 _mtlk_obsm_extract_operating_class_from_bss_info(mtlk_obss_scan_manager_t *obsm_mgr,
  mtlk_20_40_bss_info_t *bss_info)
{
  return 0;
}

static BOOL _mtlk_obsm_find_descriptor_ptr(mtlk_obss_scan_manager_t *obsm_mgr,
  uint8 operating_class, UMI_INTOLERANT_CHANNEL_DESCRIPTOR **intolerant_channel_desc)
{
  BOOL descriptor_available = FALSE;
  uint8 i;

  *intolerant_channel_desc = NULL;
  for (i = 0; i < obsm_mgr->scan_results.intolerant_channels_report.u8NumberOfIntolerantChannelDescriptors; i ++)
  {
    if (obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i].u8OperatingClass == operating_class)
    {
      *intolerant_channel_desc = &obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[i];
      descriptor_available = TRUE;
      break;
    }
  }
  if (*intolerant_channel_desc == NULL)
  {
    if (obsm_mgr->scan_results.intolerant_channels_report.u8NumberOfIntolerantChannelDescriptors < UMI_MAX_NUMBER_OF_INTOLERANT_CHANNEL_DESCRIPTORS)
    {
      *intolerant_channel_desc = &obsm_mgr->scan_results.intolerant_channels_report.sIntolerantChannelDescriptors[obsm_mgr->scan_results.intolerant_channels_report.u8NumberOfIntolerantChannelDescriptors];
      (*intolerant_channel_desc)->u8OperatingClass = operating_class;
      obsm_mgr->scan_results.intolerant_channels_report.u8NumberOfIntolerantChannelDescriptors ++;
      descriptor_available = TRUE;
    }
  }

  return descriptor_available;
}

BOOL __MTLK_IFUNC mtlk_obsm_is_scan_in_progress(mtlk_obss_scan_manager_t *obsm_mgr)
{
  return obsm_mgr->scan_in_progress;
}
