/******************************************************************************

                               Copyright (c) 2012
                            Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
/*
 * $Id$
 *
 * 
 *
 * EEPROM data processing module
 *
 * Originally written by Grygorii Strashko
 *
 */

#ifndef MTLK_EEPROM_H_
#define MTLK_EEPROM_H_

#define MTLK_MAKE_EEPROM_VERSION(major, minor) (((major) << 8) | (minor))

#define  MTLK_IDEFS_ON
#include "mtlkidefs.h"

typedef struct
{
  uint16 x;
  uint8 y;
} __MTLK_IDATA mtlk_eeprom_tpc_point_t;

typedef struct
{
  uint8 tpc_value;
  uint8 backoff_value;
  uint8 backoff_mult_value;
  mtlk_eeprom_tpc_point_t *point;
} __MTLK_IDATA mtlk_eeprom_tpc_antenna_t;

typedef struct _mtlk_eeprom_tpc_data_t
{
  struct _mtlk_eeprom_tpc_data_t *next;
  uint8 channel;
  uint16 freq;
  uint8 band;
  uint8 spectrum_mode;
  uint8 num_points;
  uint8 num_antennas;
  mtlk_eeprom_tpc_antenna_t *antenna;
} __MTLK_IDATA mtlk_eeprom_tpc_data_t;

#define   MTLK_IDEFS_OFF
#include "mtlkidefs.h"

mtlk_eeprom_tpc_data_t* __MTLK_IFUNC
mtlk_find_closest_freq (uint8 channel, mtlk_eeprom_data_t *eeprom);

uint16 __MTLK_IFUNC
mtlk_get_max_tx_power(mtlk_eeprom_data_t* eeprom, uint8 channel);

#endif /* MTLK_EEPROM_H_ */
