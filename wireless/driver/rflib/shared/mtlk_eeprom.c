/******************************************************************************

                               Copyright (c) 2012
                            Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
/*
 * $Id$
 *
 * 
 *
 * EEPROM data processing module
 *
 * Originally written by Andrii Tseglytskyi
 *
 */

#include "mtlkinc.h"
#include "mtlkerr.h"
#include "channels.h"
#include "mtlkaux.h"
#include "mtlkmib.h"

#include "eeprom.h"
#include "mtlk_eeprom.h"
#include "cis_manager.h"
#include "mtlk_tpcv4.h"
#include "mtlk_algorithms.h"
#include "mtlk_channels_propr.h"
#include "mtlk_coreui.h"
#include "mtlk_df_fw.h"
#include "mtlk_df.h"
#include "mtlkhal.h"
#include "hw_mmb.h"

#if defined(MTCFG_LINDRV_HW_PCIE) || defined (MTCFG_LINDRV_HW_PCIG3)
  #include "eeprom_gpio.h"
#endif /* defined(MTCFG_LINDRV_HW_PCIE) || defined (MTCFG_LINDRV_HW_PCIG3) */

#define LOG_LOCAL_GID   GID_EEPROM
#define LOG_LOCAL_FID   0

/*****************************************************************************
* Local type definitions
******************************************************************************/

/* \cond DOXYGEN_IGNORE */
#define  MTLK_IDEFS_ON
#define  MTLK_IDEFS_PACKING 1
#include "mtlkidefs.h"
/* \endcond */

/* From TTPCom document "PCI/CardBus Host Reference Configuration
   Hardware Specification" p. 32 */
#define MTLK_EEPROM_EXEC_SIGNATURE              0x1BFC

#define MTLK_EE_BLOCKED_SEND_TIMEOUT     (10000) /* ms */

/* PCI configuration */
typedef struct _mtlk_eeprom_pci_cfg_t {           /* len  ofs */
  uint16 eeprom_executive_signature;              /*  2    2  */
  uint16 ee_control_configuration;                /*  2    4  */
  uint16 ee_executive_configuration;              /*  2    6  */
  uint8  revision_id;                             /*  1    7  */
  uint8  class_code[3];                           /*  3   10  */
  uint8  bist;                                    /*  1   11  */
#if defined(__LITTLE_ENDIAN_BITFIELD)
  uint8  misc : 4;
  uint8  status : 4;                              /*  1   12  */
#elif defined(__BIG_ENDIAN_BITFIELD)
  uint8  status : 4;                              /*  1   12  */
  uint8  misc : 4;
#else
  # error Endianess not defined!
#endif
  uint32 card_bus_cis_pointer;                    /*  4   16  */
  uint16 subsystem_vendor_id;                     /*  2   18  */
  uint16 subsystem_id;                            /*  2   20  */
  uint8  max_lat;                                 /*  1   21  */
  uint8  min_gnt;                                 /*  1   22  */
  uint16 power_management_capabilities;           /*  2   24  */
  uint32 hrc_runtime_base_address_register_range; /*  4   28  */
  uint32 local1_base_address_register_range;      /*  4   32  */
  uint16 hrc_target_configuration;                /*  2   34  */
  uint16 hrc_initiator_configuration;             /*  2   36  */
  uint16 vendor_id;                               /*  2   38  */
  uint16 device_id;                               /*  2   40  */
  uint32 reserved[6];                             /* 24   64  */
} __MTLK_IDATA mtlk_eeprom_pci_cfg_t;

typedef struct _mtlk_eeprom_t {
  mtlk_eeprom_pci_cfg_t config_area;
  mtlk_cis_area_t cis_area;
  /* ... CIS sections ... */
} __MTLK_IDATA mtlk_eeprom_t;

#define   MTLK_IDEFS_OFF
#include "mtlkidefs.h"

#define  MTLK_IDEFS_ON
#include "mtlkidefs.h"

/* all data read from the EEPROM (except PCI configuration) as a structure */
struct _mtlk_eeprom_data_t {
  uint8 valid;
  uint16 vendor_id;
  uint16 device_id;
  uint16 sub_vendor_id;
  uint16 sub_device_id;
  mtlk_eeprom_cis_data_t  cis;
} __MTLK_IDATA;

#define   MTLK_IDEFS_OFF
#include "mtlkidefs.h"

/*****************************************************************************
* Local definitions
******************************************************************************/
#define TPC_DEBUG 0

/*****************************************************************************
* Function implementation
******************************************************************************/

mtlk_eeprom_data_t* __MTLK_IFUNC
mtlk_eeprom_create(void)
{
    mtlk_eeprom_data_t    *eeprom_data =
            mtlk_osal_mem_alloc(sizeof(mtlk_eeprom_data_t), MTLK_MEM_TAG_EEPROM);

    if (NULL != eeprom_data)
    {
        memset(eeprom_data, 0, sizeof(mtlk_eeprom_data_t));
    }

    return eeprom_data;
}

void __MTLK_IFUNC
mtlk_eeprom_delete(mtlk_eeprom_data_t *eeprom_data)
{
    if (NULL != eeprom_data)
    {
        mtlk_osal_mem_free(eeprom_data);
    }
}

/**
  Retrieve the size of useful data in EEPROM

  \param header   Handle to the EEPROM data [I]

  \return
    Size in octets of useful data
*/
static __INLINE int
_eeprom_get_data_size (mtlk_eeprom_t *header)
{
  int size;

  MTLK_ASSERT(NULL != header);

  /* CIS size - Number of bytes from address until last CIS section */
  size = MAC_TO_HOST16(header->cis_area.size);

  /* add PCI data size */
  size += sizeof(mtlk_eeprom_pci_cfg_t);
  return size;
}

static void
fill_structure_from_tpc (mtlk_eeprom_data_t *eeprom, uint8 ant_id,
                         TPC_FREQ *tpc_list, mtlk_eeprom_tpc_data_t *tpc,
                         int full_copy)
{
  mtlk_eeprom_tpc_antenna_t *antenna = tpc->antenna + ant_id;

  MTLK_UNREFERENCED_PARAM(eeprom);
  MTLK_ASSERT(tpc);
  MTLK_ASSERT(antenna);
  MTLK_ASSERT(ant_id < mtlk_eeprom_get_num_antennas(eeprom));

  memset(tpc_list, 0, sizeof(TPC_FREQ));

  tpc_list->MaxTxPowerIndex = antenna->tpc_value;

  if (full_copy) {
    uint8 i;
    tpc_list->chID = tpc->channel;
    tpc_list->Backoff = antenna->backoff_value;
    tpc_list->BackoffMultiplier = antenna->backoff_mult_value;
    ILOG5_DDDD("ch=%d, mpi=%d, backoff=%d, backoff_mult=%d", tpc_list->chID,
        tpc_list->MaxTxPowerIndex, tpc_list->Backoff, tpc_list->BackoffMultiplier);

    ILOG5_D("%d points", tpc->num_points);
    for (i = 0; i < tpc->num_points; i++) {
        tpc_list->X_axis[i] = HOST_TO_MAC16(antenna->point[i].x);
        tpc_list->Y_axis[i] = antenna->point[i].y;
        ILOG5_DDD("%d: (%u, %u)", i, antenna->point[i].x,
                                antenna->point[i].y);
      }
  }
  else {
    tpc_list->Y_axis[0] = antenna->point[0].y;
  }
}

static mtlk_eeprom_tpc_data_t*
find_struct_for_spectrum (mtlk_eeprom_tpc_data_t *tpc_list, uint16 freq, uint8 spectrum)
{
  while (tpc_list) {
    if (tpc_list->freq == freq && tpc_list->spectrum_mode == spectrum)
      return tpc_list;

    tpc_list = tpc_list->next;
  }

  return NULL;
}

static mtlk_txmm_clb_action_e __MTLK_IFUNC
mtlk_reload_tpc_cfm_clb(mtlk_handle_t clb_usr_data, mtlk_txmm_data_t* data, mtlk_txmm_clb_reason_e reason)
{
#ifdef MTCFG_DEBUG
  uint8 i = (uint8)clb_usr_data;
  UMI_MIB *pMib = (UMI_MIB *)data->payload;

  if ((pMib->u16Status == HOST_TO_MAC16(UMI_OK)) && (reason == MTLK_TXMM_CLBR_CONFIRMED)) {
    ILOG2_D("Successfully set MIB_TPC_ANT_%d", i);
  } else {
    ELOG_DD("Failed to set MIB_TPC_ANT_%d, error %d.", i,
        MAC_TO_HOST16(pMib->u16Status));
  }
#else
  MTLK_UNREFERENCED_PARAM(clb_usr_data);
  MTLK_UNREFERENCED_PARAM(data);
  MTLK_UNREFERENCED_PARAM(reason);
#endif

  return MTLK_TXMM_CLBA_FREE;
}

static uint16
_mtlk_get_tpc_mib_id_for_antenna(uint8 ant_number)
{
  switch(ant_number) {
    case 0:
      return MIB_TPC_ANT_0;
    case 1:
      return MIB_TPC_ANT_1;
    case 2:
      return MIB_TPC_ANT_2;
    default:
      MTLK_ASSERT(!"Should never be here");
      return 0;
  }
}

static int
_mtlk_set_tpc_mib(mtlk_eeprom_data_t *eeprom, mtlk_txmm_t *txmm,
                  mtlk_txmm_msg_t *man_msg,
                  uint8 ant_number, mtlk_eeprom_tpc_data_t* index_freq,
                  mtlk_eeprom_tpc_data_t *close_freq_index,
                  mtlk_eeprom_tpc_data_t *tpc)
{
  mtlk_txmm_data_t *man_entry = NULL;
  UMI_MIB *pMib;
  mtlk_eeprom_tpc_data_t *tpc1;
  List_Of_Tpc *tpc_list;
  int res;

  MTLK_ASSERT(NULL != eeprom);
  MTLK_ASSERT(NULL != txmm);
  MTLK_ASSERT(NULL != man_msg);
  MTLK_ASSERT(ant_number < mtlk_eeprom_get_num_antennas(eeprom));

  man_entry = mtlk_txmm_msg_get_empty_data(man_msg, txmm);
  if (man_entry == NULL) {
    ELOG_V("No free man slot available to set MIB_TPC_ANT_x");
    return MTLK_ERR_NO_RESOURCES;
  }

  man_entry->id = UM_MAN_SET_MIB_REQ;
  man_entry->payload_size = sizeof(UMI_MIB);
  pMib = (UMI_MIB*) man_entry->payload;

  memset(pMib, 0, sizeof(*pMib));
  pMib->u16ObjectID = HOST_TO_MAC16(_mtlk_get_tpc_mib_id_for_antenna(ant_number));

  tpc_list = &pMib->uValue.sList_Of_Tpc;

  /* fill mib value */
  if (index_freq) {
    if (index_freq->spectrum_mode) {
      fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[0], index_freq, 1);
      tpc1 = find_struct_for_spectrum(tpc, index_freq->freq, 0);
      if (tpc1)
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[2], tpc1, 1);
      else
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[2], index_freq, 0);
    } else {
      fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[2], index_freq, 1);
      tpc1 = find_struct_for_spectrum(tpc, index_freq->freq, 1);
      if (tpc1)
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[0], tpc1, 1);
      else
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[0], index_freq, 0);
    }
  }

  if (!close_freq_index)
    close_freq_index = index_freq;

  if (close_freq_index) {
    if (close_freq_index->spectrum_mode) {
      fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[1], close_freq_index, 1);
      tpc1 = find_struct_for_spectrum(tpc, close_freq_index->freq, 0);
      if (tpc1)
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[3], tpc1, 1);
      else
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[3], close_freq_index, 0);
    } else {
      fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[3], close_freq_index, 1);
      tpc1 = find_struct_for_spectrum(tpc, close_freq_index->freq, 1);
      if (tpc1)
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[1], tpc1, 1);
      else
        fill_structure_from_tpc(eeprom, ant_number, &tpc_list->sTPCerFreq[1], close_freq_index, 0);
    }
  }
  /* send data to MAC */
  res = mtlk_txmm_msg_send(man_msg, mtlk_reload_tpc_cfm_clb, HANDLE_T(ant_number) ,5000);
  if (res != MTLK_ERR_OK) {
    ELOG_D("Failed to set MIB_TPC_ANT_%d, timed-out", ant_number);
    return res;
  }

  return MTLK_ERR_OK;
};

int __MTLK_IFUNC
mtlk_reload_tpc (uint8 spectrum_mode, uint8 upper_lower, uint16 channel, mtlk_txmm_t *txmm,
                 mtlk_txmm_msg_t *man_msgs, uint32 nof_man_msgs, mtlk_eeprom_data_t *eeprom, uint8 num_tx_antennas)
{
  drv_params_t params;
  uint16 s_freq;
  uint8 ant;
  uint8 freq = channel_to_band(channel);
  uint16 minimum = 10000;
  uint16 second_minimum = 0;
  uint16 freq_delta, freq_delta1;
  int res = MTLK_ERR_UNKNOWN;
  mtlk_eeprom_tpc_data_t *tpc, *prev = NULL,
                         *close_freq_index = NULL,
                         *index_freq = NULL;

  MTLK_ASSERT(NULL != eeprom);

  if (eeprom->cis.tpc_valid == 0)
    return MTLK_ERR_EEPROM;

  params.band = freq;
  params.bandwidth = ((spectrum_mode == 1) ? 40 : 20);
  params.upper_lower = (uint8) ((spectrum_mode == 1) ? upper_lower : ALTERNATE_NONE);
  params.reg_domain = 0;
  params.spectrum_mode = spectrum_mode;

  s_freq = mtlk_calc_start_freq(&params, channel);

  if (freq == MTLK_HW_BAND_2_4_GHZ)
    tpc = eeprom->cis.tpc_24;
  else
    tpc = eeprom->cis.tpc_52;

  while (tpc) {
    freq_delta = ((s_freq > tpc->freq) ? (s_freq - tpc->freq) : (tpc->freq - s_freq)); // finds distance from center frequency.

    ILOG5_DDDDD("ch=%d, freq=%d, tpc_ch=%d, tpc_freq=%d, freq_delta=%d", channel, s_freq, tpc->channel, tpc->freq, freq_delta);

    if (prev && (prev->freq == tpc->freq)) {
      goto NEXT;
    }

    if(freq_delta < minimum) {
      second_minimum = minimum;
      close_freq_index  = index_freq;
      minimum = freq_delta;
      index_freq = tpc;
      ILOG5_DD("1: min=%d, sec_min=%d", minimum, second_minimum);
    } else if (freq_delta < second_minimum) {
      second_minimum = freq_delta;
      close_freq_index = tpc;
      ILOG5_DD("2: min=%d, sec_min=%d", minimum, second_minimum);
    }

NEXT:
    prev = tpc;
    tpc = tpc->next;
  }

  /* Sort structures on channel ID if distances to both are equal */
  if (index_freq && close_freq_index) {
    freq_delta = ((s_freq > index_freq->freq) ? (s_freq - index_freq->freq) : (index_freq->freq - s_freq));
    freq_delta1 = ((s_freq > close_freq_index->freq) ? (s_freq - close_freq_index->freq) : (close_freq_index->freq - s_freq));

    if (freq_delta == freq_delta1) {
      if (close_freq_index->channel < index_freq->channel) {
        prev = close_freq_index;
        close_freq_index = index_freq;
        index_freq = prev;
      }
    }
  }

  ILOG5_DDD("ch=%d: Closest_ch_1=%d, Closest_ch_2 ch=%d", channel,
      index_freq ? index_freq->channel : -1,
      close_freq_index ? close_freq_index->channel : -1);

  /* Fill list of closest frequencies */
  /* fill structures for CB and nCB modes.
   */
  if (freq == MTLK_HW_BAND_2_4_GHZ)
    tpc = eeprom->cis.tpc_24;
  else
    tpc = eeprom->cis.tpc_52;

  MTLK_ASSERT(man_msgs != NULL);
  MTLK_ASSERT(nof_man_msgs >= num_tx_antennas);

  for(ant = 0; ant < num_tx_antennas; ant++)
  {
    res = _mtlk_set_tpc_mib(eeprom, txmm, &man_msgs[ant], ant, index_freq, close_freq_index, tpc);
    if(MTLK_ERR_OK != res)
      return res;
  }

  return MTLK_ERR_OK;
}

/* Used only to fill scan vector FREQUENCY_ELEMENT structures with TPC data */
mtlk_eeprom_tpc_data_t* __MTLK_IFUNC
mtlk_find_closest_freq (uint8 channel, mtlk_eeprom_data_t *eeprom)
{
  uint8 band;
  uint16 freq;
  uint16 freq_diff = 0;
  uint16 minimum = 10000;

  mtlk_eeprom_tpc_data_t *tpc = NULL, *res = NULL, *list;

  MTLK_ASSERT(NULL != eeprom);

  if (eeprom->cis.tpc_valid == 0)
    return NULL;

  band = channel_to_band(channel);
  freq = channel_to_frequency(channel);

  if (band == MTLK_HW_BAND_2_4_GHZ)
    list = tpc = eeprom->cis.tpc_24;
  else
    list = tpc = eeprom->cis.tpc_52;

  // Additional check.
  // Ideally, this function shouldn't be called for band with non-existant TPC
  if (!tpc) {
    ILOG5_S("No TPC for %s", mtlk_eeprom_band_to_string(band));
    return NULL;
  }

  while (tpc) {
    freq_diff = (freq > tpc->freq) ? (freq - tpc->freq) : (tpc->freq - freq);

    ILOG5_DDDDD("ch=%d freq=%d, tpc_ch=%d tpc_freq=%d, freq_delta=%d", channel, freq, tpc->channel, tpc->freq, freq_diff);

    if (freq_diff < minimum) {
      minimum = freq_diff;
      res = tpc;
    }

    tpc = tpc->next;
  }

  if (res) {
    tpc = find_struct_for_spectrum(list, res->freq, 0);
    if( tpc )
      res = tpc;

    ILOG5_DDDD("result: ch=%d freq=%d, tpc_ch=%d tpc_freq=%d", channel, freq, res->channel, res->freq);
  }

  return res;
}

uint16 __MTLK_IFUNC
mtlk_get_max_tx_power(mtlk_eeprom_data_t* eeprom, uint8 channel)
{
  uint16 tpc_tx_power = (uint16) -1;

  mtlk_eeprom_tpc_data_t *tpc =
    mtlk_find_closest_freq(channel, eeprom);

  MTLK_ASSERT(NULL != eeprom);

  if (NULL != tpc) {
    int ant;
    for(ant = 0; ant < mtlk_eeprom_get_num_antennas(eeprom); ant++)
      if (tpc_tx_power > (uint16)tpc->antenna[ant].point[0].y)
        tpc_tx_power = (uint16)tpc->antenna[ant].point[0].y;
  }

  return tpc_tx_power;
}

static const mtlk_ability_id_t _eeprom_abilities[] = {
  MTLK_CORE_REQ_GET_EEPROM_CFG,
  MTLK_CORE_REQ_GET_EE_CAPS
};

/**
  Cleanup EEPROM data and remove EEPROM abilities from
  the ability manager.

  \param vap        Handle to VAP [I]
  \param ee_data    Pointer to the structured view of EEPROM [O]

  \return
    MTLK_ERR...
*/
void __MTLK_IFUNC
mtlk_clean_eeprom_data(mtlk_vap_handle_t vap, mtlk_eeprom_data_t *eeprom_data)
{
  int i;

  MTLK_ASSERT(NULL != eeprom_data);
  MTLK_ASSERT(NULL != vap);

  for(i = 0; i < ARRAY_SIZE(_eeprom_abilities); i++)
  {
    mtlk_abmgr_disable_ability(mtlk_vap_get_abmgr(vap), _eeprom_abilities[i]);
    mtlk_abmgr_unregister_ability(mtlk_vap_get_abmgr(vap), _eeprom_abilities[i]);
  }

  mtlk_eeprom_cis_data_clean(&eeprom_data->cis);

  memset(&eeprom_data, 0, sizeof(eeprom_data));
}

/*****************************************************************************
**
** NAME         _eeprom_parse
**
** PARAMETERS   eeprom_data           pointer to eeprom data struc to fill in
**              raw_eeprom            pointer to the buffer to parse
**
** RETURNS      MTLK_ERR...
**
** DESCRIPTION  This function called to perform parsing of EEPROM
**
******************************************************************************/
static int
_eeprom_parse (mtlk_eeprom_t *raw_eeprom, mtlk_eeprom_data_t *parsed_eeprom)
{
  int res = MTLK_ERR_EEPROM;

  MTLK_ASSERT(NULL != parsed_eeprom);

  /* Verify EEPROM signature */
  if (raw_eeprom->config_area.eeprom_executive_signature != HOST_TO_MAC16(MTLK_EEPROM_EXEC_SIGNATURE))
  {
    ELOG_D("Invalid EEPROM Executive Signature: 0x%04X. "
           "Default parameters are used.",
           MAC_TO_HOST16(raw_eeprom->config_area.eeprom_executive_signature));
    return MTLK_ERR_EEPROM;
  }

  /* Parse EEPROM */
  parsed_eeprom->vendor_id = raw_eeprom->config_area.vendor_id;
  parsed_eeprom->sub_vendor_id = raw_eeprom->config_area.subsystem_vendor_id;
  parsed_eeprom->device_id = raw_eeprom->config_area.device_id;
  parsed_eeprom->sub_device_id = raw_eeprom->config_area.subsystem_id;

  res = mtlk_cis_area_parse(&raw_eeprom->cis_area, &parsed_eeprom->cis);
  if (MTLK_ERR_OK != res)
    return res;

  return MTLK_ERR_OK;
}

#ifdef EEPROM_DATA_VALIDATION

#define _MTLK_MAX_EEPROM_PART_SIZE   (sizeof(((UMI_GENERIC_MAC_REQUEST *)NULL)->data))

/**
  Read parto of EEPROM data to buffer via FW

  \param txmm     TXMM API [I]
  \param offset   Offset of source buffer [I]
  \param size     Number of bytes to read [I]
  \param buffer   Buffer to read to [O]

  \return
    MTLK_ERR... or number of bytes written to buffer
*/
static int
_eeprom_read_from_fw_part(mtlk_txmm_t *txmm, int offset, int size, char *buffer)
{
  UMI_GENERIC_MAC_REQUEST* psEepromReq;
  int result = MTLK_ERR_OK;
  mtlk_txmm_msg_t   man_msg;
  mtlk_txmm_data_t* man_data;
  int bytes_read = 0;

  MTLK_ASSERT(NULL != txmm);
  MTLK_ASSERT(NULL != buffer);
  MTLK_ASSERT(_MTLK_MAX_EEPROM_PART_SIZE >= size);

  man_data = mtlk_txmm_msg_init_with_empty_data(&man_msg, txmm, NULL);
  if (NULL == man_data) {
    ELOG_V("Can't read EEPROM due to lack of MAN_MSG");
    result = MTLK_ERR_EEPROM;
    goto FINISH;
  }

  man_data->id           = UM_MAN_GENERIC_MAC_REQ;
  man_data->payload_size = sizeof(*psEepromReq);

  psEepromReq            = (UMI_GENERIC_MAC_REQUEST*)man_data->payload;
  psEepromReq->opcode    = HOST_TO_MAC32(MAC_EEPROM_REQ);
  psEepromReq->size      = HOST_TO_MAC32(size);
  psEepromReq->action    = HOST_TO_MAC32(MT_REQUEST_SET);
  psEepromReq->res0      = HOST_TO_MAC32(offset);
  psEepromReq->res1      = 0;
  psEepromReq->res2      = 0;
  psEepromReq->retStatus = 0;

  ILOG5_DD("Request EEPROM read: from %d, %d byte(s)", offset, size);

  if (mtlk_txmm_msg_send_blocked(&man_msg, MTLK_EE_BLOCKED_SEND_TIMEOUT) != MTLK_ERR_OK) {
    ELOG_V("Failed to read EEPROM, timed-out");
    result = MTLK_ERR_EEPROM;
    goto FINISH;
  }

  if (HOST_TO_MAC32(UMI_OK) == psEepromReq->retStatus) {
    bytes_read = size;
  } else if (HOST_TO_MAC32(EEPROM_ILLEGAL_ADDRESS) == psEepromReq->retStatus) {
    bytes_read = HOST_TO_MAC32(psEepromReq->res2) - offset;
  } else {
    ELOG_D("Failed to read EEPROM, MAC error %d", MAC_TO_HOST32(psEepromReq->retStatus));
    result = MTLK_ERR_EEPROM;
    goto FINISH;
  }

  ILOG5_DDD("Read %d bytes from EEPROM, starting at offset %d, read return status is %d",
       bytes_read, offset, MAC_TO_HOST32(psEepromReq->retStatus));

  MTLK_ASSERT(bytes_read <= size);
  memcpy(buffer, psEepromReq->data, bytes_read);
  result = MTLK_ERR_OK;

FINISH:
  if (man_data)
    mtlk_txmm_msg_cleanup(&man_msg);

  return MTLK_SUCCESS(result) ? bytes_read : result;
}

/**
  Read EEPROM data to buffer via FW

  \param vap          Handle to VAP [I]
  \param buffer       Buffer to read to [O]
  \param buffer_size  Size of available buffer [I]
  \param bytes_read   Number of bytes was read [O]

  \return
    MTLK_ERR...
*/
static int
_eeprom_read_from_fw(mtlk_vap_handle_t vap, void* buffer, uint32 buffer_size, int* bytes_read)
{
  int res = 0;

  *bytes_read = 0;

  do {
    MTLK_ASSERT(*bytes_read <= buffer_size);

    res = _eeprom_read_from_fw_part(mtlk_vap_get_txmm(vap), *bytes_read,
                                    _MTLK_MAX_EEPROM_PART_SIZE,
                                    (uint8*)buffer + *bytes_read);

    if(MTLK_FAILURE(res))
      return res;

    *bytes_read += res;
  } while(_MTLK_MAX_EEPROM_PART_SIZE == res);

  return MTLK_ERR_OK;
}

/**
  Read EEPROM data in to the buffer from FW

  \param vap    Handle to VAP [I]
  \param buf    Pointer to the buffer [O]
  \param len    Size of available buffer [I]

  \return
    MTLK_ERR...
*/
int __MTLK_IFUNC
mtlk_eeprom_get_raw_fw(mtlk_vap_handle_t vap, uint8 *buf, uint32 len)
{
  void *buffer = NULL;
  int eeprom_total_size = 0;
  int res = MTLK_ERR_OK;

  MTLK_ASSERT(NULL != buf);
  MTLK_ASSERT(NULL != vap);
  MTLK_ASSERT(len >= MTLK_MAX_EEPROM_SIZE);

  buffer = mtlk_osal_mem_alloc(MTLK_MAX_EEPROM_SIZE, MTLK_MEM_TAG_EEPROM);
  if(NULL == buffer)
  {
    ELOG_V("Failed to allocate memory for EEPROM");
    return MTLK_ERR_NO_MEM;
  }

  res = _eeprom_read_from_fw(vap, buffer, MTLK_MAX_EEPROM_SIZE, &eeprom_total_size);

  if ((MTLK_ERR_OK == res) && (len >= eeprom_total_size))
  {
    memcpy(buf, buffer, eeprom_total_size);
  }
  else
  {
    res = MTLK_ERR_PARAMS;
  }

  mtlk_osal_mem_free(buffer);
  return res;
}

static const mtlk_ability_id_t _eeprom_fw_abilities[] = {
  MTLK_CORE_REQ_GET_EEPROM_FW
};

int __MTLK_IFUNC
mtlk_eeprom_fw_access_init(mtlk_vap_handle_t vap)
{
  int res;

  res = mtlk_abmgr_register_ability_set(mtlk_vap_get_abmgr(vap),
    _eeprom_fw_abilities, 1);

  if(MTLK_ERR_OK == res)
  {
    mtlk_abmgr_enable_ability_set(mtlk_vap_get_abmgr(vap),
      _eeprom_fw_abilities, 1);
  }

  return res;
}

void __MTLK_IFUNC
mtlk_eeprom_fw_access_cleanup(mtlk_vap_handle_t vap)
{
  mtlk_abmgr_disable_ability(mtlk_vap_get_abmgr(vap), _eeprom_fw_abilities[0]);
  mtlk_abmgr_unregister_ability(mtlk_vap_get_abmgr(vap), _eeprom_fw_abilities[0]);
}


#endif /* EEPROM_DATA_VALIDATION */

#if defined(MTCFG_LINDRV_HW_PCIE) || defined (MTCFG_LINDRV_HW_PCIG3)
/**
  Read EEPROM data to buffer via GPIO

  \param vap          Handle to VAP [I]
  \param buffer       Buffer to read to [O]
  \param buffer_size  Size of available buffer [I]
  \param bytes_read   Number of bytes was read [O]

  \return
    MTLK_ERR...
*/
static int
_eeprom_read_from_gpio(mtlk_vap_handle_t vap, void* buffer, uint32 buffer_size, int* bytes_read)
{
  mtlk_ccr_t *ccr = NULL;
  uint32 eeprom_size = 0;
  int res = 0;

  *bytes_read = 0;

  mtlk_vap_get_hw_vft(vap)->get_prop(vap, MTLK_HW_PROP_CCR,
                                     &ccr, sizeof(&ccr));

  res = eeprom_gpio_init(ccr, &eeprom_size);
  if (MTLK_ERR_OK != res)
  {
    ELOG_V("Unable to initialize access to EEPROM via GPIO");
    return res;
  }

  ILOG2_D("EEPROM via GPIO available (size:%d)", eeprom_size);

  MTLK_ASSERT(buffer_size >= eeprom_size);

  if(buffer_size > eeprom_size)
    buffer_size = eeprom_size;

  res = eeprom_gpio_read(ccr, 0, buffer, buffer_size);
  if (MTLK_ERR_OK != res)
  {
    ELOG_V("Unable to read data from EEPROM via GPIO");
  }
  else
  {
    *bytes_read = buffer_size;

    if (((uint16*)buffer)[0] != HOST_TO_MAC16(MTLK_EEPROM_EXEC_SIGNATURE))
    {
      ELOG_V("Invalid data received from EEPROM");
      res = MTLK_ERR_EEPROM;
    }
  }

  eeprom_gpio_clean(ccr);

  return res;
}
#endif /* defined(MTCFG_LINDRV_HW_PCIE) || defined (MTCFG_LINDRV_HW_PCIG3) */

/**
  Check CRC field in EEPROM data

  \param raw_eeprom

  \return
    MTLK_ERR...
*/
static int
_eeprom_crc32_validate (mtlk_eeprom_t const *raw_eeprom)
{
  int res = MTLK_ERR_EEPROM;
  uint16 crc_chunk1_len = 0;
  uint16 crc_chunk2_len = 0;
  uint32 crc = 0, eeprom_crc = 0;

  MTLK_ASSERT(NULL != raw_eeprom);

  res = mtlk_cis_crc_parse(&raw_eeprom->cis_area, &eeprom_crc, &crc_chunk1_len, &crc_chunk2_len);
  if (MTLK_ERR_OK != res)
    return res;

  if (0 == crc_chunk1_len)
  {
    /* No CRC section in the CIS area */
    return MTLK_ERR_OK;
  }

  eeprom_crc = MAC_TO_HOST32(eeprom_crc);

  crc = mtlk_osal_crc32(~0L, (void*)raw_eeprom,
                        sizeof(mtlk_eeprom_pci_cfg_t) + crc_chunk1_len);
  crc = mtlk_osal_crc32(crc, (void*)((uint8*)raw_eeprom + sizeof(mtlk_eeprom_pci_cfg_t) + crc_chunk1_len + sizeof(eeprom_crc)),
                         crc_chunk2_len);
  crc ^= ~0L;

  if (crc != eeprom_crc)
  {
    ELOG_DD("Invalid EEPROM Checksum: 0x%08X (0x%08X)",
            crc, eeprom_crc);
    return MTLK_ERR_EEPROM;
  }

  return MTLK_ERR_OK;
}

/**
  Read EEPROM data to buffer from file

  \param vap          Handle to VAP [I]
  \param buffer       Buffer to read to [O]
  \param buffer_size  Size of available buffer [I]
  \param bytes_read   Number of bytes was read [O]

  \return
    MTLK_ERR...
*/
static int
_eeprom_read_from_file(mtlk_vap_handle_t vap, char *buffer, uint32 buffer_size, int *bytes_read)
{
  int res = MTLK_ERR_OK;
  mtlk_df_fw_file_buf_t fb;
  mtlk_vap_manager_t *vap_manager = NULL;
  int eeprom_data_size = 0;
  char fname[0x20];

  vap_manager = mtlk_vap_get_manager(vap);

  mtlk_vap_get_hw_vft(vap)->get_prop(vap, MTLK_HW_EEPROM_NAME,
                                     fname, sizeof(fname));

  res = mtlk_df_fw_load_file(vap_manager, fname, &fb);
  if (res != MTLK_ERR_OK)
    return res;

  if (buffer_size < fb.size)
  {
    ELOG_DD("Unexpected size of EEPROM data: "
            "received (%d) more then allowed (%d)",
            fb.size, buffer_size);
    res = MTLK_ERR_EEPROM;
    goto ERROR;
  }

  if (fb.size < sizeof(mtlk_eeprom_t))
  {
    ELOG_D("Invalid size of EEPROM data (%d)", fb.size);
    res = MTLK_ERR_EEPROM;
    goto ERROR;
  }

  if (((uint16*)fb.buffer)[0] != HOST_TO_MAC16(MTLK_EEPROM_EXEC_SIGNATURE))
  {
    ELOG_V("Invalid data received from EEPROM");
    res = MTLK_ERR_EEPROM;
    goto ERROR;
  }

  eeprom_data_size = _eeprom_get_data_size((void*)fb.buffer);

  if(eeprom_data_size > fb.size)
  {
    ELOG_DD("EEPROM contains invalid data: "
      "data size (%d) is bigger than total size (%d)",
      eeprom_data_size, fb.size);
    res = MTLK_ERR_EEPROM;
    goto ERROR;
  }

  res = _eeprom_crc32_validate((void*)fb.buffer);
  if (MTLK_ERR_OK != res)
    goto ERROR;

  memcpy (buffer, fb.buffer, fb.size);
  *bytes_read = fb.size;

ERROR:
  mtlk_df_fw_unload_file(vap_manager, &fb);

  return res;
}

/**
  Read EEPROM data to buffer

  \param vap          Handle to VAP [I]
  \param buffer       Handle to buffer holder [O]
  \param bytes_read   Number of bytes was read [O]

  \return
    MTLK_ERR...
*/
static int
_eeprom_read_raw_data(mtlk_vap_handle_t vap, void** buffer, int* bytes_read)
{
  int res = 0;
  mtlk_card_type_t card_type = MTLK_CARD_UNKNOWN;
  
  int i;
  unsigned int *tptr;
  

  MTLK_ASSERT(NULL != vap);
  MTLK_ASSERT(NULL != buffer);
  MTLK_ASSERT(NULL != bytes_read);

  *bytes_read = 0;

  res = mtlk_vap_get_hw_vft(vap)->get_prop(vap, MTLK_HW_PROP_CARD_TYPE,
                                           &card_type, sizeof(&card_type));
  if (MTLK_ERR_OK != res)
  {
    return MTLK_ERR_NOT_SUPPORTED;
  }

  *buffer = mtlk_osal_mem_alloc(MTLK_MAX_EEPROM_SIZE, MTLK_MEM_TAG_EEPROM);
  if(NULL == *buffer)
  {
    ELOG_V("Failed to allocate memory for EEPROM");
    return MTLK_ERR_NO_MEM;
  }

  res = _eeprom_read_from_file(vap, *buffer, MTLK_MAX_EEPROM_SIZE, bytes_read);
  if(MTLK_ERR_OK != res)
  {
    CARD_SELECTOR_START(card_type);
    IF_CARD_AHBG35( res = MTLK_ERR_EEPROM /* todo: read from eFUSE */);
    IF_CARD_G3    ( res = _eeprom_read_from_gpio(vap, *buffer, MTLK_MAX_EEPROM_SIZE, bytes_read));
    CARD_SELECTOR_END();
  }

tptr=*buffer;
pr_info("eeprom dump %i %i: \n",(*bytes_read), MTLK_MAX_EEPROM_SIZE);
for (i=0;i<(*bytes_read)/4;i+=8) {
	pr_info("%08x %08x %08x %08x  %08x %08x %08x %08x\n",
		tptr[i + 0],
		tptr[i + 1],
		tptr[i + 2],
		tptr[i + 3],
		tptr[i + 4],
		tptr[i + 5],
		tptr[i + 6],
		tptr[i + 7]
	);
}

  if(MTLK_ERR_OK != res)
  {
    mtlk_osal_mem_free(*buffer);
    *buffer = NULL;

    ELOG_V("Failed to read EEPROM");
  }
  else
  {
    ILOG5_D("EEPROM reading finished, %d bytes read.", *bytes_read);
  }

  return res;
}

char* __MTLK_IFUNC
mtlk_eeprom_band_to_string(unsigned band)
{
  switch (band) {
  case MTLK_HW_BAND_5_2_GHZ:
    return "5.2";
  case MTLK_HW_BAND_2_4_GHZ:
    return "2.4";
  case MTLK_HW_BAND_BOTH:
    return "Dual";
  default:
    return "Unknown";
  }
}

uint32 __MTLK_IFUNC
mtlk_eeprom_get_xtal_value(const mtlk_eeprom_data_t *ee_data)
{
  MTLK_ASSERT(NULL != ee_data);

  if (ee_data->valid)
    return (uint32)MAC_TO_HOST16(ee_data->cis.xtal);

  return (uint32)XTAL_DEFAULT_VALUE;
}

int8 __MTLK_IFUNC
mtlk_eeprom_get_lna_gain_bypass(const mtlk_eeprom_data_t *ee_data)
{
  MTLK_ASSERT(NULL != ee_data);

  if (ee_data->valid)
    return ee_data->cis.lna.lna_gain_bypass;

  return LNA_GAIN_BYPASS_DEFAULT_VALUE;
}

int8 __MTLK_IFUNC
mtlk_eeprom_get_lna_gain_high(const mtlk_eeprom_data_t *ee_data)
{
  MTLK_ASSERT(NULL != ee_data);

  if (ee_data->valid)
    return ee_data->cis.lna.lna_gain_high;

  return LNA_GAIN_HIGH_DEFAULT_VALUE;
}

int __MTLK_IFUNC
mtlk_eeprom_is_band_supported(const mtlk_eeprom_data_t *ee_data, unsigned band)
{
  MTLK_ASSERT(NULL != ee_data);

  if ((band == MTLK_HW_BAND_BOTH && ee_data->cis.tpc_24 && ee_data->cis.tpc_52) ||
      (band == MTLK_HW_BAND_2_4_GHZ && ee_data->cis.tpc_24) ||
      (band == MTLK_HW_BAND_5_2_GHZ && ee_data->cis.tpc_52))
    return MTLK_ERR_OK;

  return MTLK_ERR_UNKNOWN;
}

int __MTLK_IFUNC
mtlk_eeprom_is_band_valid(const mtlk_eeprom_data_t *ee_data, unsigned band)
{
  MTLK_ASSERT(NULL != ee_data);

  if ((band == MTLK_HW_BAND_2_4_GHZ && ee_data->cis.tpc_24) ||
      (band == MTLK_HW_BAND_5_2_GHZ && ee_data->cis.tpc_52))
    return MTLK_ERR_OK;

  return MTLK_ERR_UNKNOWN;
}

/**
  Read EEPROM data in to the driver friendly structure
  and add EEPROM abilities in to the ability manager.

  \param vap        Handle to VAP [I]
  \param ee_data    Pointer to the structured view of EEPROM [O]

  \return
    Error is handled by "mtlk_eeprom_is_valid" function
*/
int __MTLK_IFUNC
mtlk_eeprom_read_and_parse(mtlk_vap_handle_t vap, mtlk_eeprom_data_t *ee_data)
{
  int eeprom_data_size, result, full_eeprom_size;
  void* raw_eeprom;

  MTLK_ASSERT(NULL != vap);
  MTLK_ASSERT(NULL != ee_data);

  /* assume we have no valid EEPROM */
  ee_data->valid = 0;

  result = mtlk_abmgr_register_ability_set(mtlk_vap_get_abmgr(vap),
    _eeprom_abilities, ARRAY_SIZE(_eeprom_abilities));
  if(MTLK_ERR_OK != result) {
    return MTLK_ERR_EEPROM;
  }

  result = _eeprom_read_raw_data(vap, &raw_eeprom, &full_eeprom_size);
  if(MTLK_ERR_OK != result) {
    ELOG_V("Failed to read data from EEPROM");
    goto RETOK;
  }

  if(full_eeprom_size < sizeof(mtlk_eeprom_t)) {
    ELOG_D("Full EEPROM size is %d bytes, which is smaller than EEPROM header. Can not continue.",
          full_eeprom_size);
    result = MTLK_ERR_EEPROM;
    goto ERROR;
  }

  eeprom_data_size = _eeprom_get_data_size(raw_eeprom);

  if(eeprom_data_size > full_eeprom_size) {
    ELOG_DD("EEPROM contains invalid data: "
      "data size (%d) is bigger than total size (%d)",
      eeprom_data_size, full_eeprom_size);
    result = MTLK_ERR_EEPROM;
    goto ERROR;
  }

  result = _eeprom_parse(raw_eeprom, ee_data);
  if (MTLK_ERR_OK != result) {
    ELOG_D("EEPROM parsing failed with code %d", result);
    goto ERROR;
  }

  mtlk_abmgr_enable_ability_set(mtlk_vap_get_abmgr(vap),
    _eeprom_abilities, ARRAY_SIZE(_eeprom_abilities));

  /* mark data as valid */
  ee_data->valid = 1;

ERROR:
  mtlk_osal_mem_free(raw_eeprom);
RETOK:
  /* always return OK */
  return MTLK_ERR_OK;
}

void __MTLK_IFUNC mtlk_eeprom_get_cfg(mtlk_eeprom_data_t *eeprom, mtlk_eeprom_data_cfg_t *cfg)
{
  MTLK_ASSERT(NULL != eeprom);
  MTLK_ASSERT(NULL != cfg);

  cfg->eeprom_version = eeprom->cis.version;
  memcpy(cfg->mac_address, eeprom->cis.card_id.mac_address, MTLK_EEPROM_SN_LEN);
  cfg->country_code = eeprom->cis.card_id.country_code;
  cfg->type = eeprom->cis.card_id.type;
  cfg->revision = eeprom->cis.card_id.revision;
  cfg->vendor_id = MAC_TO_HOST16(eeprom->vendor_id);
  cfg->device_id = MAC_TO_HOST16(eeprom->device_id);
  cfg->sub_vendor_id = MAC_TO_HOST16(eeprom->sub_vendor_id);
  cfg->sub_device_id = MAC_TO_HOST16(eeprom->sub_device_id);
  memcpy(cfg->sn, eeprom->cis.card_id.sn, MTLK_EEPROM_SN_LEN);
  cfg->production_week = eeprom->cis.card_id.production_week;
  cfg->production_year = eeprom->cis.card_id.production_year;
}

/**
  Read EEPROM data in to the buffer

  \param vap    Handle to VAP [I]
  \param buf    Pointer to the buffer [O]
  \param len    Size of available buffer [I]

  \return
    MTLK_ERR...
*/
int __MTLK_IFUNC
mtlk_eeprom_get_raw_cfg(mtlk_vap_handle_t vap, uint8 *buf, uint32 len)
{
  void *data = NULL;
  int eeprom_total_size = 0;
  int res = MTLK_ERR_OK;

  MTLK_ASSERT(NULL != buf);
  MTLK_ASSERT(NULL != vap);
  MTLK_ASSERT(len >= MTLK_MAX_EEPROM_SIZE);

  res = _eeprom_read_raw_data(vap, &data, &eeprom_total_size);
  if(MTLK_ERR_OK != res)
  {
    return res;
  }

  if (len >= eeprom_total_size)
  {
    memcpy(buf, data, eeprom_total_size);
  }
  else
  {
    res = MTLK_ERR_PARAMS;
  }

  mtlk_osal_mem_free(data);
  return res;
}

uint32 __MTLK_IFUNC mtlk_eeprom_get_size(void)
{
  return sizeof(mtlk_eeprom_t);
}

int __MTLK_IFUNC
mtlk_eeprom_get_caps (const mtlk_eeprom_data_t *eeprom, mtlk_clpb_t *clpb)
{
  int res;

  mtlk_eeprom_data_stat_entry_t stats;

  MTLK_ASSERT(NULL != eeprom);

  stats.ap_disabled = eeprom->cis.card_id.dev_opt_mask.s.ap_disabled;
  stats.disable_sm_channels = eeprom->cis.card_id.dev_opt_mask.s.disable_sm_channels;

  res = mtlk_clpb_push(clpb, &stats, sizeof(stats));
  if (MTLK_ERR_OK != res) {
    mtlk_clpb_purge(clpb);
  }

  return res;
}

 uint8 __MTLK_IFUNC
mtlk_eeprom_get_nic_type(mtlk_eeprom_data_t *eeprom_data)
{
  MTLK_ASSERT(NULL != eeprom_data);
  return eeprom_data->cis.card_id.type;
}

uint8 __MTLK_IFUNC
mtlk_eeprom_get_nic_revision(mtlk_eeprom_data_t *eeprom_data)
{
    MTLK_ASSERT(NULL != eeprom_data);
    return eeprom_data->cis.card_id.revision;
}

const uint8* __MTLK_IFUNC
mtlk_eeprom_get_nic_mac_addr(mtlk_eeprom_data_t *eeprom_data)
{
    MTLK_ASSERT(NULL != eeprom_data);
    return eeprom_data->cis.card_id.mac_address;
}

uint8 __MTLK_IFUNC
mtlk_eeprom_get_country_code(mtlk_eeprom_data_t *eeprom_data)
{
  MTLK_ASSERT(NULL != eeprom_data);
  if (eeprom_data->valid)
    return eeprom_data->cis.card_id.country_code;
  else
    return 0;
}

uint8 __MTLK_IFUNC
mtlk_eeprom_get_num_antennas(mtlk_eeprom_data_t *eeprom)
{
  MTLK_ASSERT(eeprom->cis.version >= MTLK_MAKE_EEPROM_VERSION(4,0));

  return NUM_TX_ANTENNAS_GEN3;
}

int __MTLK_IFUNC
mtlk_eeprom_is_valid(const mtlk_eeprom_data_t *ee_data)
{
    MTLK_ASSERT(NULL != ee_data);
    return (ee_data->valid) ? MTLK_ERR_OK : MTLK_ERR_UNKNOWN;
}

uint8 __MTLK_IFUNC
mtlk_eeprom_get_disable_sm_channels(mtlk_eeprom_data_t *eeprom)
{
    MTLK_ASSERT(NULL != eeprom);
    return eeprom->cis.card_id.dev_opt_mask.s.disable_sm_channels;
}

uint16 __MTLK_IFUNC
mtlk_eeprom_get_vendor_id(mtlk_eeprom_data_t *eeprom)
{
    MTLK_ASSERT(NULL != eeprom);
    return eeprom->vendor_id;
}

uint16 __MTLK_IFUNC
mtlk_eeprom_get_device_id(mtlk_eeprom_data_t *eeprom)
{
    MTLK_ASSERT(NULL != eeprom);
    return eeprom->device_id;
}

int __MTLK_IFUNC
mtlk_set_mib_eeprom_info(mtlk_txmm_t* txmm, mtlk_eeprom_data_t *eeprom)
{
  MIB_VALUE uValue;

  if (MTLK_ERR_OK != mtlk_eeprom_is_valid(eeprom))
    return MTLK_ERR_OK;

  /* Send ee_version to MAC */
  memset(&uValue, 0, sizeof(MIB_VALUE));
  uValue.sEepromInfo.u16EEPROMVersion = cpu_to_le16(eeprom->cis.version);

  if (eeprom->cis.tpc_52) {
    uValue.sEepromInfo.u8NumberOfPoints5GHz = eeprom->cis.tpc_52->num_points;
  }

  if (eeprom->cis.tpc_24) {
    uValue.sEepromInfo.u8NumberOfPoints2GHz = eeprom->cis.tpc_24->num_points;
  }

  if (MTLK_ERR_OK != mtlk_set_mib_value_raw(txmm, MIB_EEPROM_VERSION, &uValue)) {
    ELOG_V("set MIB_EEPROM_VERSION failed");
    return MTLK_ERR_UNKNOWN;
  }

  return MTLK_ERR_OK;
}

int __MTLK_IFUNC
mtlk_eeprom_check_ee_data(mtlk_eeprom_data_t *eeprom, mtlk_txmm_t* txmm_p, BOOL is_ap)
{
  MTLK_ASSERT(NULL != eeprom);

  if (MTLK_ERR_OK == mtlk_eeprom_is_valid(eeprom))
  {
    /* Check EEPROM options mask */
    ILOG0_D("Options mask is 0x%02x", eeprom->cis.card_id.dev_opt_mask.d);

    if (eeprom->cis.card_id.dev_opt_mask.s.ap_disabled && is_ap) {
      ELOG_V("AP functionality is not available on this device");
      goto err_ap_func_is_not_available;
    }

    if (mtlk_eeprom_get_disable_sm_channels(eeprom)) {
      ILOG0_V("DFS (SM-required) channels will not be used");
    }

    if (MTLK_ERR_OK != mtlk_set_mib_eeprom_info(txmm_p, eeprom)) {
      goto err_set_mib_eeprom_info;
    }
  }
  return MTLK_ERR_OK;

err_set_mib_eeprom_info:
err_ap_func_is_not_available:
  return MTLK_ERR_UNKNOWN;
}
