/******************************************************************************

                               Copyright (c) 2012
                            Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
/*
 * Accessor to the Card Information Section from EEPROM
 */

#include "mtlkinc.h"
#include "mtlkerr.h"
#include "channels.h"
#include "mtlkaux.h"

#include "mtlk_eeprom.h"
#include "cis_manager.h"

#include "mtlk_tpcv4.h"
#include "mtlk_algorithms.h"
#include "mtlk_channels_propr.h"
#include "mtlk_coreui.h"
#include "mtlk_df.h"

#define LOG_LOCAL_GID   GID_CIS
#define LOG_LOCAL_FID   0

/*****************************************************************************
* Local type definitions
******************************************************************************/

/* \cond DOXYGEN_IGNORE */
#define  MTLK_IDEFS_ON
#define  MTLK_IDEFS_PACKING 1
#include "mtlkidefs.h"
/* \endcond */

/* CIS: TPC */
typedef struct
{
    uint8 size_24;
    uint8 size_52;
    /* ... TPC data ... */
}__MTLK_IDATA tpc_header_t;

/* Card setions TPL codes */
typedef enum {
  CIS_TPL_CODE_CID    = 0x80,
  CIS_TPL_CODE_TPCG3  = 0x83,
  CIS_TPL_CODE_LNA    = 0x82,
  CIS_TPL_CODE_XTAL   = 0x84,
  CIS_TPL_CODE_CRC    = 0x85,
  CIS_TPL_CODE_NONE   = 0xFF
} cis_tpl_code_t;

/* Card Information Structure header */
typedef struct
{
    uint8 tpl_code; /** see \ref cis_tpl_code_t */
    uint8 data_size;
    /* ... CIS data ... */
}__MTLK_IDATA cis_header_t;

#define   MTLK_IDEFS_OFF
#include "mtlkidefs.h"

/*****************************************************************************
* Local definitions
******************************************************************************/
#define TPC_DEBUG 0

/*****************************************************************************
* Function implementation
******************************************************************************/

static void
_tpc_data_free(mtlk_eeprom_tpc_data_t *data)
{
  int i;

  if (NULL == data)
    return;

  if (NULL != data->antenna)
  {
    for(i = 0; i < data->num_antennas; i++)
    {
      if (NULL != data->antenna[i].point)
      {
        mtlk_osal_mem_free(data->antenna[i].point);
        data->antenna[i].point = NULL;
      }
    }

    mtlk_osal_mem_free(data->antenna);
    data->antenna = NULL;
  }

  mtlk_osal_mem_free(data);
}

static mtlk_eeprom_tpc_data_t *
_tpc_data_alloc(uint8 num_points, uint8 num_antennas)
{
  int i;
  mtlk_eeprom_tpc_data_t *data = NULL;

  data = mtlk_osal_mem_alloc(sizeof(mtlk_eeprom_tpc_data_t),
                             MTLK_MEM_TAG_TPC_DATA);
  if (NULL == data)
    return NULL;

  memset(data, 0, sizeof(mtlk_eeprom_tpc_data_t));
  data->num_points = num_points;
  data->num_antennas = num_antennas;

  data->antenna = mtlk_osal_mem_alloc(
    sizeof(mtlk_eeprom_tpc_antenna_t) * num_antennas,
    MTLK_MEM_TAG_TPC_ANTENNA);

  if (NULL == data->antenna)
    goto ERROR;

  memset(data->antenna, 0,
         sizeof(mtlk_eeprom_tpc_antenna_t) * num_antennas);

  for(i = 0; i < num_antennas; i++)
  {
    data->antenna[i].point = mtlk_osal_mem_alloc(
      sizeof(mtlk_eeprom_tpc_point_t) * num_points,
      MTLK_MEM_TAG_TPC_POINT);

    if (NULL == data->antenna[i].point)
      goto ERROR;

    memset(data->antenna[i].point, 0,
           sizeof(mtlk_eeprom_tpc_point_t) * num_points);
  }

  return data;

ERROR:
  _tpc_data_free(data);

  return NULL;
}

/**
  This function tries to find a CIS (Card information
  section) in the buffer specified

  \param id         CIS block id to search for [I]
  \param cis        pointer to the CIS buffer [I]
  \param cis_size   size of CIS buffer in bytes [I]
  \param data_size  returns the size of CIS data [O]
  \param next_cis   pointer to the next CIS buffer [O]

  \return
    NULL if CIS block not found, otherwise pointer to the CIS data
*/
static void*
_cis_data_find (uint8 id, cis_header_t *cis, int cis_size, uint8 *data_size, void** next_cis)
{
  uint8 *data = NULL;

  /* The header starts with two bytes defining the size of the           */
  /* CIS section in bytes (not including these bytes and the next four). */
  /* A value of 0h will be interpreted as "no CIS exist"                 */
  /* Next comes TPL_LINK byte, defining number of bytes (N) exists       */
  /*  at this CIS structure (not including the current byte).            */

  MTLK_ASSERT(NULL != cis);
  MTLK_ASSERT(NULL != data_size);

  while ((cis_size >= sizeof(cis_header_t)) && (cis->tpl_code != CIS_TPL_CODE_NONE))
  {
    data = (uint8*) cis;
    data += sizeof(cis_header_t);

    if (cis->data_size + sizeof(cis_header_t) > cis_size)
    {
      ELOG_DP("Partial CIS[0x%02X] detected at 0x%p",
        cis->tpl_code, cis);
      return NULL;
    }

    if (cis->tpl_code == id)
    {
      if (next_cis)
        *next_cis = data + cis->data_size;

      *data_size = cis->data_size;

      return data;
    }

    cis_size -= cis->data_size + sizeof(cis_header_t);

    cis = (cis_header_t *) (data + cis->data_size);
  }

  return NULL;
}

/**
  This function tries to find end of CIS (Card information
  section) in the buffer specified

  \param cis        pointer to the CIS buffer [I]
  \param cis_size   size of CIS buffer in bytes [I]

  \return
    NULL if CIS end not found not found, otherwise pointer to the END of CIS data
*/
static void*
_cis_find_end (cis_header_t *cis, int cis_size)
{
  uint8 *data = NULL;

  /* The header starts with two bytes defining the size of the           */
  /* CIS section in bytes (not including these bytes and the next four). */
  /* A value of 0h will be interpreted as "no CIS exist"                 */
  /* Next comes TPL_LINK byte, defining number of bytes (N) exists       */
  /*  at this CIS structure (not including the current byte).            */

  MTLK_ASSERT(NULL != cis);

  while (cis_size >= sizeof(cis_header_t))
  {
    data = (uint8*) cis;

    if (cis->tpl_code == CIS_TPL_CODE_NONE)
    {
      return data;
    }

    data += sizeof(cis_header_t);

    if (cis->data_size + sizeof(cis_header_t) > cis_size)
    {
      ELOG_DP("Partial CIS[0x%02X] detected at 0x%p",
        cis->tpl_code, cis);
      return NULL;
    }

    cis_size -= cis->data_size + sizeof(cis_header_t);

    cis = (cis_header_t *) (data + cis->data_size);
  }

  return NULL;
}

/**
  Perform data coping of TPC CIS item ver 4

  \param item   pointer to the TPC item [I]
  \param data   pointer to eeprom TPC data struct to fill in [O]
*/
static void
_parse_tpc4_item_data (mtlk_tpcv4_t *item, mtlk_eeprom_tpc_data_t *data)
{
  int ant_id, pnt_id;

  data->channel = mtlk_tpcv4_get_channel(item);
  data->band = (mtlk_tpcv4_get_band(item) == MTLK_TPC_BAND_2GHZ) ? 1 : 0;
  data->spectrum_mode = (mtlk_tpcv4_get_cb_flag(item) == MTLK_TPC_CB) ? 1 : 0;;
  data->freq = channel_to_frequency(data->channel) + (data->spectrum_mode ? 10: 0);

  for(ant_id = 0; ant_id < data->num_antennas; ant_id++)
  {
    data->antenna[ant_id].tpc_value =
      mtlk_tpcv4_get_tpc_val(item, ant_id);

    data->antenna[ant_id].backoff_value =
      mtlk_tpcv4_get_backoff_packed(item, ant_id);

    data->antenna[0].backoff_mult_value =
      mtlk_tpcv4_get_backoff_mul(item, ant_id);

    for(pnt_id = 0; pnt_id < data->num_points; pnt_id++)
    {
        mtlk_tpc_point_t point;
        mtlk_tpcv4_get_point(item, ant_id, pnt_id, &point);
        data->antenna[ant_id].point[pnt_id].x = point.x;
        data->antenna[ant_id].point[pnt_id].y = point.y;
    }
  }

  ILOG5_DDD("TPC: ch=%d, band=%d, spectrum=%d", data->channel, data->band, data->spectrum_mode);
}

/**
  Perform parsing of TPC CIS item ver 4

  \param item         pointer to the TPC item [I]
  \param parsed_cis   pointer to eeprom CIS data struct to fill in [O]

  \return
    MTLK_ERR_OK on success or error value from \ref mtlk_error_t
*/
static int
_parse_tpc4_item (mtlk_tpcv4_t *item, mtlk_eeprom_cis_data_t *parsed_cis)
{
  mtlk_eeprom_tpc_data_t *data =
    _tpc_data_alloc(mtlk_tpcv4_get_num_points(item),
    NUM_TX_ANTENNAS_GEN3 /* TODO: Use info from configuration file */);

  if (!data)
  {
    ELOG_V("Failed to allocate TPC data structure");
    return MTLK_ERR_NO_MEM;
  }

  ILOG5_D("TPC with %d points", data->num_points);

  _parse_tpc4_item_data(item, data);

  if (data->band)
  {
    data->next = parsed_cis->tpc_24;
    parsed_cis->tpc_24 = data;
  }
  else
  {
    data->next = parsed_cis->tpc_52;
    parsed_cis->tpc_52 = data;
  }

#if TPC_DEBUG
  {
    int i, j;
    for (i = 0; i < data->num_points; i++)
      for(j = 0; j < data->num_antennas; j++)
        ILOG5_DDDD("ant%d: point_%d: (%u, %u)", j, i,
                    data->antenna[j].point[i].x,
                    data->antenna[j].point[i].y);
  }
#endif
  return MTLK_ERR_OK;
}

/**
  Cleanup enumerator for TPC list
*/
static void
_tpc_list_clean(mtlk_eeprom_tpc_data_t *tpc)
{
  mtlk_eeprom_tpc_data_t *prev;

  while (tpc) {
    prev = tpc;
    tpc = tpc->next;
    _tpc_data_free(prev);
  }
}

/**
  TCP data list accessor
*/
static void* _tpc_list_get_next(void* item)
{
  return (void*) ((mtlk_eeprom_tpc_data_t*)item)->next;
}

/**
  TPC data list updater
*/
static void _tpc_list_set_next(void* item, void* next)
{
  ((mtlk_eeprom_tpc_data_t*)item)->next = (mtlk_eeprom_tpc_data_t*) next;
}

/**
  TPC data frequency comparator
*/
static int _tpc_list_is_less(void* item1, void* item2)
{
  return ((mtlk_eeprom_tpc_data_t*)item1)->freq
    < ((mtlk_eeprom_tpc_data_t*)item2)->freq;
}

/**
  TPC list sort routine
*/
static void
_tpc_list_sort(mtlk_eeprom_tpc_data_t** list)
{
  mtlk_sort_slist(
    (void**)list,
    _tpc_list_get_next,
    _tpc_list_set_next,
    _tpc_list_is_less);
}

/**
  Perform parsing of TPC CIS block ver 4

  \param tpc          pointer to the buffer with TPC items [I]
  \param tpc_size     total length of the TPC CIS [I]
  \param parsed_cis   pointer to eeprom data struct to fill in [O]

  \return
    MTLK_ERR_OK on success or error value from \ref mtlk_error_t
*/
static int
_parse_tpc4 (tpc_header_t *tpc, int tpc_size,
             mtlk_eeprom_cis_data_t *parsed_cis)
{
  uint8 data_size = 0;
  uint8 *data = NULL;
  mtlk_tpcv4_t tpc4_parser;

  data = (uint8*)tpc + sizeof(tpc_header_t);

  /* calculate TPC data size */
  tpc_size -= sizeof(tpc_header_t);

  ILOG5_DD("size_24=%d, size_52=%d", tpc->size_24, tpc->size_52);

  while (tpc_size > 0)
  {
    MTLK_TPC_BAND band;
    int err;

    band = mtlk_tpcv4_get_band_by_raw_buffer(data);

    MTLK_ASSERT( (MTLK_TPC_BAND_2GHZ == band) ||
                 (MTLK_TPC_BAND_5GHZ == band) );

    data_size = (MTLK_TPC_BAND_2GHZ == band) ?
                    tpc->size_24 : tpc->size_52;

    ILOG5_DD("Current TPC section offset = %u, left CIS size = %d",
             data - (uint8*)tpc, tpc_size);

    if (tpc_size < data_size)
    {
      ELOG_DD("TPC CIS contains partial structure: "
              "expected size %d, left in CIS area: %d",
              data_size, tpc_size);
      return MTLK_ERR_NO_ENTRY;
    }

    err = mtlk_tpcv4_init(&tpc4_parser, data, data_size);
    if (MTLK_ERR_OK != err)
    {
      ELOG_V("Failed to create TPC4 parser object");
      return err;
    }

    err = _parse_tpc4_item(&tpc4_parser, parsed_cis);

    mtlk_tpcv4_cleanup(&tpc4_parser);

    if (MTLK_ERR_OK != err)
    {
      ELOG_V("Failed to parse TPC4 item");
      return err;
    }

    /* next item */
    tpc_size -= data_size;
    data += data_size;
  } /* while */

  return MTLK_ERR_OK;
}

/**
  This routine provide parsing of CIS area from EEPROM

  \param cis_area     Pointer to CIS area [I]
  \param parsed_cis   Pointer to the buffer for parsed data [O]

  \return
    MTLK_ERR_OK on success or error value from \ref mtlk_error_t
*/
int __MTLK_IFUNC
mtlk_cis_area_parse (mtlk_cis_area_t const *const cis_area,
                     mtlk_eeprom_cis_data_t *parsed_cis)
{
  void *cis = NULL;
  void *cis_end = NULL;
  uint8 cis_size;
  tpc_header_t *tpc = NULL;
  mtlk_cis_cardid_t *card_id = NULL;
  uint16 *xtal_data = NULL;
  mtlk_lna_cis_data_t *lna_data = NULL;

  MTLK_ASSERT(cis_area);
  MTLK_ASSERT(parsed_cis);

  parsed_cis->tpc_valid = 0;

  /* Current implementation supports EEPROM versions 4 */
  if (cis_area->version1 != 4)
  {
    ELOG_DD("Unsupported version of the EEPROM: %u.%u",
            cis_area->version1, cis_area->version0);
    return MTLK_ERR_NOT_SUPPORTED;
  }

  parsed_cis->version = MTLK_MAKE_EEPROM_VERSION(
                          cis_area->version1,
                          cis_area->version0);

  cis = (((char*)cis_area) + sizeof(mtlk_cis_area_t));
  cis_end = cis + MAC_TO_HOST16(cis_area->size) - sizeof(mtlk_cis_area_t);

  card_id = _cis_data_find(CIS_TPL_CODE_CID, cis, cis_end - cis,
                           &cis_size, NULL);
  if (!card_id)
  {
    ELOG_V("Can not find Card ID CIS");
    return MTLK_ERR_EEPROM;
  }

  if (cis_size != sizeof(*card_id))
  {
    ELOG_V("Incorrect size of Card ID CIS");
    return MTLK_ERR_EEPROM;
  }

  /* check card_id values */
  if ((card_id->revision < 'A') || (card_id->revision > 'Z'))
  {
    ELOG_DC("Invalid Card Revision: 0x%02x (%c)",
          (int)card_id->revision,
          (char)card_id->revision);
    return MTLK_ERR_EEPROM;
  }

  /* Handle optional LNA configuration */
  lna_data = _cis_data_find(CIS_TPL_CODE_LNA, cis, cis_end - cis,
                             &cis_size, NULL);
  if (NULL != lna_data)
  {
    if (cis_size != sizeof(parsed_cis->lna))
    {
      ELOG_V("Incorrect size of LNA CIS");
      return MTLK_ERR_EEPROM;
    }
    /* copy LNA value */
    memcpy(&parsed_cis->lna, lna_data, sizeof(parsed_cis->lna));
  }
  else
  {
    parsed_cis->lna.lna_gain_bypass = LNA_GAIN_BYPASS_DEFAULT_VALUE;
    parsed_cis->lna.lna_gain_high   = LNA_GAIN_HIGH_DEFAULT_VALUE;
  }

  /* Handle optional XTAL configuration */
  xtal_data = _cis_data_find(CIS_TPL_CODE_XTAL, cis, cis_end - cis,
                             &cis_size, NULL);
  if (NULL != xtal_data)
  {
    /* The Xtal section may be longer and include
       additional byte for Temperature sensor */
    if (cis_size < sizeof(parsed_cis->xtal))
    {
      ELOG_V("Incorrect size of XTAL CIS");
      return MTLK_ERR_EEPROM;
    }

    /* copy Xtal value */
    memcpy(&parsed_cis->xtal, xtal_data, sizeof(parsed_cis->xtal));
  }
  else
  {
    parsed_cis->xtal = HOST_TO_MAC16(XTAL_DEFAULT_VALUE);
  }

  /* CIS may contain multiple TPC areas, */
  /* we have to parse all of them.              */
  tpc = _cis_data_find(CIS_TPL_CODE_TPCG3, cis, cis_end - cis,
                       &cis_size, &cis);
  if (NULL == tpc)
  {
    ELOG_V("At least one TPC CIS should be present");
    return MTLK_ERR_EEPROM;
  }

  do
  {
    if (MTLK_ERR_OK != _parse_tpc4(tpc, cis_size, parsed_cis))
    {
      mtlk_eeprom_cis_data_clean(parsed_cis);
      return MTLK_ERR_EEPROM;
    }

    tpc = _cis_data_find(CIS_TPL_CODE_TPCG3, cis, cis_end - cis,
                         &cis_size, &cis);
  } while(NULL != tpc);

  _tpc_list_sort(&parsed_cis->tpc_24);
  _tpc_list_sort(&parsed_cis->tpc_52);

  parsed_cis->tpc_valid = 1;

  parsed_cis->card_id = *card_id;

  /* If country code not recognized - indicate *unknown* country */
  if (country_code_to_domain(parsed_cis->card_id.country_code) == 0)
  {
    parsed_cis->card_id.country_code = 0;
  }

  return MTLK_ERR_OK;
}

/**
  Cleanup routing for parsed CIS data

  \param parsed_cis   pointer to eeprom CIS data struct [I]
*/
void __MTLK_IFUNC
mtlk_eeprom_cis_data_clean (mtlk_eeprom_cis_data_t *parsed_cis)
{
  _tpc_list_clean(parsed_cis->tpc_24);
  _tpc_list_clean(parsed_cis->tpc_52);
}

/**
  This routine retrieve CRC value from CIS area of EEPROM

  \param cis_area     Pointer to CIS area [I]
  \param crc_value    Handle to the buffer for the CRC value
                      stored in the CIS area [O]
  \param data_len     Handle to the buffer for the length of CIS area data
                      used for CRC calculation [O]
                      Zero - if no CRC in the CIS area

  \return
    MTLK_ERR_OK on success or error value from \ref mtlk_error_t
*/
int __MTLK_IFUNC
mtlk_cis_crc_parse (mtlk_cis_area_t const *const cis_area,
                    uint32 *crc_value, uint16 *chunk1_len, uint16 *chunk2_len)
{
  void *cis = NULL;
  void *cis_end = NULL;
  uint8 cis_size;
  uint32 *eeprom_crc = NULL;
  uint32 *eeprom_cis_end = NULL;

  MTLK_ASSERT(cis_area);
  MTLK_ASSERT(crc_value);
  MTLK_ASSERT(chunk1_len);
  MTLK_ASSERT(chunk2_len);

  *chunk1_len = 0;
  *chunk2_len = 0;

  /* Current implementation supports EEPROM versions 4 */
  if (cis_area->version1 != 4)
  {
    ELOG_DD("Unsupported version of the EEPROM: %u.%u",
            cis_area->version1, cis_area->version0);
    return MTLK_ERR_NOT_SUPPORTED;
  }

  cis = (((char*)cis_area) + sizeof(mtlk_cis_area_t));
  cis_end = cis + MAC_TO_HOST16(cis_area->size) - sizeof(mtlk_cis_area_t);

  /* Handle optional CRC configuration */
  eeprom_crc = _cis_data_find(CIS_TPL_CODE_CRC, cis, cis_end - cis,
                             &cis_size, NULL);
  if (NULL == eeprom_crc)
    return MTLK_ERR_OK;

  if (cis_size != sizeof(*crc_value))
  {
    ELOG_V("Incorrect size of CRC CIS");
    return MTLK_ERR_EEPROM;
  }

  /* copy CRC value */
  memcpy(crc_value, eeprom_crc, sizeof(*crc_value));

  /* The whole CIS area are involved for CRC calculation, including
     CIS Header for CRC.
     NOTE: CRC section could not be the last one in CIS area. */
  *chunk1_len = (uint16)((void*)eeprom_crc - (void*)cis_area);

  eeprom_cis_end = _cis_find_end(cis, cis_end - cis);

  if (eeprom_cis_end == NULL)
  {
    ELOG_V("Incorrect eeprom, no CIS END");
    return MTLK_ERR_EEPROM;
  }

  *chunk2_len = (uint16)((void*)eeprom_cis_end - ((void*)eeprom_crc + cis_size));

  return MTLK_ERR_OK;
}

