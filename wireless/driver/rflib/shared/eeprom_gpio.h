/******************************************************************************

                               Copyright (c) 2012
                            Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
/*
 * $Id: $
 *
 *  Read/Write data into the EEPROM.
 */
#ifndef __EEPROM_GPIO_H__
#define __EEPROM_GPIO_H__

#if !defined(MTCFG_PLATFORM_GEN35FPGA)
  int __MTLK_IFUNC
  eeprom_gpio_init (mtlk_ccr_t *ccr, uint32 *size);

  void __MTLK_IFUNC
  eeprom_gpio_clean (mtlk_ccr_t *ccr);

  int __MTLK_IFUNC
  eeprom_gpio_read(mtlk_ccr_t *ccr, uint32 offset, void *buffer, int len);

  int __MTLK_IFUNC
  eeprom_gpio_write(mtlk_ccr_t *ccr, uint32 offset, void const *buffer, int len);
#else /* MTCFG_PLATFORM_GEN35FPGA */
  static __INLINE int
  eeprom_gpio_init (mtlk_ccr_t *ccr, uint32 *size)
  {
    MTLK_UNREFERENCED_PARAM(ccr);
    MTLK_UNREFERENCED_PARAM(size);
    return MTLK_ERR_NOT_SUPPORTED;
  }

  static __INLINE void
  eeprom_gpio_clean (mtlk_ccr_t *ccr)
  {
    MTLK_UNREFERENCED_PARAM(ccr);
  }

  static __INLINE int
  eeprom_gpio_read(mtlk_ccr_t *ccr, uint32 offset, void *buffer, int len)
  {
    MTLK_UNREFERENCED_PARAM(ccr);
    return MTLK_ERR_NOT_SUPPORTED;
  }

  static __INLINE int
  eeprom_gpio_write(mtlk_ccr_t *ccr, uint32 offset, void const *buffer, int len)
  {
    MTLK_UNREFERENCED_PARAM(ccr);
    return MTLK_ERR_NOT_SUPPORTED;
  }
#endif /* MTCFG_PLATFORM_GEN35FPGA */

#endif /* __EEPROM_GPIO_H__ */
