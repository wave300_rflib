/******************************************************************************

                               Copyright (c) 2012
                            Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
#ifndef __CIS_MANAGER__
#define __CIS_MANAGER__

#define  MTLK_IDEFS_ON
#define  MTLK_IDEFS_PACKING 1
#include "mtlkidefs.h"

/**
  Generic CIS memory area structure
 */
typedef struct {
  /** Number of bytes from address until last CIS section */
  uint16 size;
  /** Base address the CIS sections will be loaded to */
  uint32 base_address; /* FIXME: to be ready for unaligned address */
  /** Minor version*/
  uint8 version0;
  /** Major version*/
  uint8 version1;
} __MTLK_IDATA mtlk_cis_area_t;


/* NOTE:
 * all cards that are not updated will have 0x42 value by default
 */
typedef union
{
  struct {
#if defined(__LITTLE_ENDIAN_BITFIELD)
    uint8 ap_disabled:1;
    uint8 disable_sm_channels:1;
    uint8 __reserved:6;
#elif defined(__BIG_ENDIAN_BITFIELD)
    uint8 __reserved:6;
    uint8 disable_sm_channels:1;
    uint8 ap_disabled:1;
#else
  # error Endianess not defined!
#endif
  } __MTLK_IDATA s;
  uint8 d;
} __MTLK_IDATA mtlk_cis_dev_opt_mask_t;

/* CIS: card ID */
typedef struct {
  uint8 type;
  uint8 revision;
  uint8 country_code;
  mtlk_cis_dev_opt_mask_t dev_opt_mask;
  uint8 rf_chip_number;
  uint8 mac_address[ETH_ALEN];
  uint8 sn[MTLK_EEPROM_SN_LEN];
  uint8 production_week;
  uint8 production_year;
} __MTLK_IDATA mtlk_cis_cardid_t;

#define   MTLK_IDEFS_OFF
#include "mtlkidefs.h"

#define  MTLK_IDEFS_ON
#include "mtlkidefs.h"

typedef struct {
  int8   lna_gain_bypass;
  int8   lna_gain_high;
} mtlk_lna_cis_data_t;

/* CIS data read from the EEPROM as a structure */
typedef struct {
  uint16                 version;
  uint16                 xtal;
  mtlk_lna_cis_data_t    lna;
  mtlk_cis_cardid_t      card_id;
  mtlk_eeprom_tpc_data_t *tpc_24;
  mtlk_eeprom_tpc_data_t *tpc_52;
  uint8                  tpc_valid;
} __MTLK_IDATA mtlk_eeprom_cis_data_t;

#define   MTLK_IDEFS_OFF
#include "mtlkidefs.h"

#define XTAL_DEFAULT_VALUE             (0x008C)
#define LNA_GAIN_BYPASS_DEFAULT_VALUE       (0)
#define LNA_GAIN_HIGH_DEFAULT_VALUE         (0)

int __MTLK_IFUNC
mtlk_cis_area_parse (mtlk_cis_area_t const *const cis_area,
                     mtlk_eeprom_cis_data_t *parsed_cis);

void __MTLK_IFUNC
mtlk_eeprom_cis_data_clean (mtlk_eeprom_cis_data_t *parsed_cis);

int __MTLK_IFUNC
mtlk_cis_crc_parse (mtlk_cis_area_t const *const cis_area,
                    uint32 *crc_value, uint16 *chunk1_len, uint16 *chunk2_len);

#endif /* __CIS_MANAGER__ */
